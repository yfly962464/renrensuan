<?php
namespace App\Controller;
use Think\Controller;
class MessageController extends Controller {
    //客户查看是否有未读消息
    //http://localhost/renren/index.php/App/Message/message_count/user_id2/3
    //{"code":1,"message_count":"5"}
    public function message_count() {
        $user_id2 = I('user_id2');
        if ($user_id2 == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        $message_count = M('message')->where("message_user_id2=$user_id2 and message_state=0")->count();
        echo json_encode(array('code'=>1,'message_count'=>$message_count));
    }

    //客户发表私信
    //http://localhost/renren/index.php/App/Message/message_add/user_id2/3/user_id/5/message_content/%E6%88%91%E5%93%AD%E4%BA%86
    //{"code":1,"data2":{"message_user_id":"5","message_user_id2":"3","message_content":"\u6211\u54ed\u4e86","message_date":1453429052}}
    public function message_add() {

        $user_id = I('user_id');
        $user_id2 = I('user_id2');
        $message_content = I('message_content');
        if ($user_id == '' || $user_id2 == '' || $message_content == '') {
            echo json_encode(array('code'=>2));
            exit;
        }        

        $check = M('check')->where("check_user_id=$user_id and check_user_id2=$user_id2")->find();

        if ($check) {
            $data2['message_user_id'] = $user_id;
            $data2['message_user_id2'] = $user_id2;
            $data2['message_content'] = $message_content;
            $user_identity_id = 0;

            $data2['message_status'] = $user_identity_id;

            $data2['message_date'] = time();
            if (M('message')->add($data2)) {
                echo json_encode(array('code'=>1,'data2'=>$data2));
            } else {
                echo json_encode(array('code'=>0));
            }
        } else {
            $data['check_user_id'] = $user_id;
            $data['check_user_id2'] = $user_id2;
            if (M('check')->add($data)) {
                $data2['message_user_id'] = $user_id;
                $data2['message_user_id2'] = $user_id2;
                $data2['message_content'] = $message_content;
                $data2['message_status'] = 0;
                $data2['message_date'] = time();
                if (M('message')->add($data2)) {
                    echo json_encode(array('code'=>1,'data2'=>$data2));
                } else {
                    echo json_encode(array('code'=>0));
                }
            } else {
                echo json_encode(array('code'=>0));
            }
        }
    }

    private function judgeUserCategory($user_id){

        $user = M('user')
                ->field('user_identity_id')
                ->where('user_id='.$user_id)->find();
 
        return $user['user_identity_id'];
    }

    //客户看大师
    //http://localhost/renren/index.php/App/Message/message_list/user_id2/3
    //{"code":1,"message_list":[{"check_user_id":"2","user":{"user_username":"\u5218\u4e9a\u73b2","user_pic":"20160120112529585.png"},"info":{"message_content":"\u90a3\u662f\u4ec0\u4e48\uff1f","message_date":"1453370046"},"count":"1"},{"check_user_id":"4","user":{"user_username":"\u5218\u5f3a\u4e1c","user_pic":"pic.jpg"},"info":{"message_content":"\u6211\u662f\u4ec0\u4e48\uff1f","message_date":"1453370499"},"count":"0"},{"check_user_id":"5","user":{"user_username":"\u9a6c\u5316\u817e","user_pic":"pic.jpg"},"info":{"message_content":"\u6211\u6709\u4ec0\u4e48\uff1f","message_date":"1453370537"},"count":"1"}]}
    public function message_list() {
        $user_id2 = I('user_id2');
        if ($user_id2 == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        $check = M('check')->field('check_user_id')->where("check_user_id2=$user_id2")->select();


        //print_R($check);die;
        if (!empty($check)) {
            foreach ($check as $key => $value) {
                $check[$key]['user'] = M('user')->field('user_username,user_pic')->where('user_id='.$value['check_user_id'])->find();
                $check[$key]['info'] = M('message')->field('message_content,message_date')->where("message_user_id2=$user_id2 and message_user_id='".$value['check_user_id']."'")->order('message_id desc')->find();
                $check[$key]['count'] = M('message')->field('message_content,message_date')->where("message_user_id2=$user_id2 and message_user_id='".$value['check_user_id']."' and message_status=1 and message_state=0")->count();
            }
            echo json_encode(array('code'=>1,'message_list'=>$check));
        } else {
            echo json_encode(array('code'=>0));
        }
    }

    //客户查看与其中一位大师的私信
    //http://localhost/renren/index.php/App/Message/message_show/user_id2/3/user_id/2
    //{"code":1,"message_show":[{"message_user_id":"2","message_content":"\u8fd9\u662f\u4ec0\u4e48\uff1f","message_status":"0","message_date":"1453370077","shenfen":"\u6211"},{"message_user_id":"2","message_content":"\u90a3\u662f\u4ec0\u4e48\uff1f","message_status":"1","message_date":"1453370046","shenfen":"\u5218\u4e9a\u73b2"},{"message_user_id":"2","message_content":"\u4f60\u662f\u4ec0\u4e48\uff1f","message_status":"0","message_date":"1453370477","shenfen":"\u6211"}]}
    public function message_show() {
        $user_id = I('user_id');
        $user_id2 = I('user_id2');
        if ($user_id == '' || $user_id2 == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        
        $u = M('message');
        $data['message_state'] = '1';
        $u->where("message_user_id2=$user_id2 and message_user_id=$user_id and message_state=0 and message_status=1")->save($data);

        $message = $u
        ->field('message_user_id,message_content,message_status,message_date')
        ->where("message_user_id2=$user_id2 and message_user_id=$user_id")
        ->order('message_id asc')
        ->select();
        if (!empty($message)) {
            foreach ($message as $key => $value) {
                if ($value['message_status'] == '0') {
                    $message[$key]['shenfen'] = '我';
                } else {
                    $message[$key]['shenfen'] = M('user')->where("user_id=".$value['message_user_id'])->getField('user_username');
                }
            }
        }
        if ($message) {
            echo json_encode(array('code'=>1,'message_show'=>$message));
        } else {
             if (empty($message)) {
                echo json_encode(array('code'=>3));
                exit();                
            }
            echo json_encode(array('code'=>0));
        }
    }

    //大师查看是否有未读消息
    //http://localhost/renren/index.php/App/Message/message_count2/user_id/2
    //{"code":1,"message_count2":"2"}
    public function message_count2() {
        $user_id = I('user_id');
        if ($user_id == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        $message_count2 = M('message')->where("message_user_id=$user_id and message_state=0")->count();
        echo json_encode(array('code'=>1,'message_count2'=>$message_count2));
    }

    //大师给客户发私信
    //http://localhost/renren/index.php/App/Message/message_add/user_id2/3/user_id/5/message_content/%E6%88%91%E5%93%AD%E4%BA%86
    //{"code":1,"data2":{"message_user_id":"5","message_user_id2":"3","message_content":"\u6211\u54ed\u4e86","message_status":"1","message_date":1453432358}}
    public function message_add2() {
        $user_id = I('user_id');
        $user_id2 = I('user_id2');
        $message_content = I('message_content');
        if ($user_id == '' || $user_id2 == '' || $message_content == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        $check = M('check')->where("check_user_id=$user_id and check_user_id2=$user_id2")->find();
        if ($check) {
            $data2['message_user_id'] = $user_id;
            $data2['message_user_id2'] = $user_id2;
            $data2['message_content'] = $message_content;
            $data2['message_status'] = '1';
            $data2['message_date'] = time();
            if (M('message')->add($data2)) {
                echo json_encode(array('code'=>1,'data2'=>$data2));
            } else {
                echo json_encode(array('code'=>0));
            }
        } else {
            $data['check_user_id'] = $user_id;
            $data['check_user_id2'] = $user_id2;
            if (M('check')->add($data)) {
                $data2['message_user_id'] = $user_id;
                $data2['message_user_id2'] = $user_id2;
                $data2['message_content'] = $message_content;
                $data2['message_status'] = '1';
                $data2['message_date'] = time();
                if (M('message')->add($data2)) {
                    echo json_encode(array('code'=>1,'data2'=>$data2));
                } else {
                    echo json_encode(array('code'=>0));
                }
            } else {
                echo json_encode(array('code'=>0));
            }
        }
    }

    //大师查看跟自己沟通过的客户
    //http://localhost/renren/index.php/App/Message/message_list2/user_id/5
    //{"code":1,"message_list":[{"check_user_id2":"3","user":{"user_username":"\u9a6c\u4e91","user_pic":"pic.jpg"},"info":{"message_content":"\u4f60\u662f\u4ec0\u4e48\uff1f","message_date":"1453370477"},"count":"2"}]}
    public function message_list2() {
        $user_id = I('user_id');
        if ($user_id == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        $check = M('check')->field('check_user_id2')->where("check_user_id=$user_id")->select();
        if (!empty($check)) {
            foreach ($check as $key => $value) {
                $check[$key]['user'] = M('user')->field('user_username,user_pic')->where('user_id='.$value['check_user_id2'])->find();
                $check[$key]['info'] = M('message')->field('message_content,message_date')->where("message_user_id=$user_id and message_user_id2='".$value['check_user_id2']."'")->order('message_date desc')->find();
                $check[$key]['count'] = M('message')->field('message_content,message_date')->where("message_user_id=$user_id and message_user_id2='".$value['check_user_id2']."' and message_status=0 and message_state=0")->count();
            }
            echo json_encode(array('code'=>1,'message_list'=>$check));
        } else {
            echo json_encode(array('code'=>0));
        }
    }

    //大师查看与其中一位客户的私信
    //http://localhost/renren/index.php/App/Message/message_show2/user_id2/3/user_id/2
    //{"code":1,"message_show2":[{"message_user_id":"2","message_content":"\u8fd9\u662f\u4ec0\u4e48\uff1f","message_status":"0","message_date":"1453370077","shenfen":"\u5218\u4e9a\u73b2"},{"message_user_id":"2","message_content":"\u90a3\u662f\u4ec0\u4e48\uff1f","message_status":"1","message_date":"1453370046","shenfen":"\u6211"},{"message_user_id":"2","message_content":"\u4f60\u662f\u4ec0\u4e48\uff1f","message_status":"0","message_date":"1453370477","shenfen":"\u5218\u4e9a\u73b2"}]}
    public function message_show2() {
        $user_id = I('user_id');
        $user_id2 = I('user_id2');
        if ($user_id == '' || $user_id2 == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        
        $u = M('message');
        $data['message_state'] = '1';
        $u->where("message_user_id2=$user_id2 and message_user_id=$user_id and message_state=0 and message_status=0")->save($data);

        $message = $u
        ->field('message_user_id,message_content,message_status,message_date')
        ->where("message_user_id2=$user_id2 and message_user_id=$user_id")
        ->order('message_id asc')
        ->select();
        if (!empty($message)) {
            foreach ($message as $key => $value) {
                if ($value['message_status'] == '1') {
                    $message[$key]['shenfen'] = '我';
                } else {
                    $message[$key]['shenfen'] = M('user')->where("user_id=".$value['message_user_id'])->getField('user_username');
                }
            }
        }
        if ($message) {
            echo json_encode(array('code'=>1,'message_show2'=>$message));
        } else {
            echo json_encode(array('code'=>0));
        }
    }
}