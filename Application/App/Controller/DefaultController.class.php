<?php
namespace App\Controller;
use Think\Controller;
import("Org.Util.Easemob");
class DefaultController extends Controller {

    var $options = array(

        'client_id' => 'YXA6NOdKUNteEeWN2q0JyRreYg',
        'client_secret' => 'YXA6ulVy04afB-feVWZW4CzMdDekKjo',
        'org_name' => '2011-fangzhou',
        'app_name' => 'forecast',

    );

	public function online() {

        $user_id = I('user_id');
        $user_online = I('user_online');

        if ($user_id == '' || $user_online == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        $data['user_online'] = $user_online;
        $h = new \Org\Util\Easemob($this->options);

        if (M('user')->where("user_id=$user_id")->save($data) !== false) {
                echo json_encode(array('code'=>1));
        } else {
            echo json_encode(array('code'=>0));
        }

    }

    private function getUserStatusOnHuanXin($user_id,$h){

        $user = M('user')
            ->field('user_tel')->where("user_id=$user_id")
            ->select();
        // get user_tel
        $user_tel = $user[0]['user_tel']; 
        // charge user's status on huanxin
        $status = $h->isOnline($user_tel);

        return $status;

    }

    private function hasUserStatusRecords($user_id,$h){

        return (null != $this->getUserStatusOnHuanXin($user_id,$h)) ? true : false;
    }

	//
    public function info() {
        $img = $_REQUEST['img'];
        $extension = $_REQUEST['extension'];
        $user_id = I('user_id');
        $user_username = I('user_username');
        $user_sex = I('user_sex');
        $user_broken = I('user_broken');
        $user_speciality = I('user_speciality');
        $user_content = I('user_content');
        if($img == '' || $extension == '' || $user_id == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        $img = explode(',', $img);
        $img_arr = array();
        foreach ($img as $key => $value) {
            $arr = base64_decode(str_replace(' ','+',$value));
            $filePath = './uploads/'.date('Ym').'/';
            if(!is_dir($filePath)){
               $this->create_folders($filePath);
            }
            $img_name = time().$key.'_APP.'.$extension;
            $filename = $filePath.$img_name;
            $fp = fopen("$filename", "w+");
            if(!is_writable($filename)){
                  echo json_encode(array('code'=>3));
                  exit;
            }
            fclose($fp);
            if(is_writable($filename) == false) {
                echo json_encode(array('code'=>4));
                exit;
            }
            $remove = file_put_contents($filename, $arr);
            if($remove == false) {
                echo json_encode(array('code'=>0));
                exit;
            }
            $img_arr[] = $filename;
        }
        if(!empty($img_arr)) {
            $data['user_username'] = $user_username;
            $data['user_sex'] = $user_sex;
            $data['user_broken'] = $user_broken;
            $data['user_speciality'] = $user_speciality;
            $data['user_content'] = $user_content;
            $data['user_content_pic'] = implode(',', $img_arr);
            if (M('user')->where("user_id=$user_id")->save($data)) {
                echo json_encode(array('code'=>1,'user_content_pic'=>$img_arr));
            } else {
                echo json_encode(array('code'=>0));
            }
        }else{
            echo json_encode(array('code'=>0));
        }
    }
    public function create_folders($dir) {
        return is_dir($dir) or ($this->create_folders(dirname($dir)) and mkdir($dir, 0777));
    }
    public function bind() {
        $user_id = I('user_id');
        $user_name = I('user_name');
        $user_card = I('user_card');
        $user_ccb = I('user_ccb');
        if($user_id == '' || $user_name == '' || $user_card == '' || $user_ccb == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        $data['user_name'] = $user_name;
        $data['user_card'] = $user_card;
        $data['user_ccb'] = $user_ccb;
        if (M('user')->where("user_id=$user_id")->save($data)) {
            echo json_encode(array('code'=>1));
			exit();
        } else {
            echo json_encode(array('code'=>0));
			exit();
        }
    }

	//我收藏的文章
	public function collection_article(){

		$user_id = I("user_id");
		$page = I('page');
        $pagesize = I('pagesize');
        if($page<=1){
            $page = 1;
        }
        if($pagesize<=0){
            $pagesize = 10;
        }
		$article=M("scwz")
			    ->field('article_id,article_title,article_content,article_pic,article_status,article_date,user_username')
			    ->join("d_article on scwz_article_id=article_id")
			    ->join('d_user on article_user_id=user_id')
			    ->where("scwz_user_id2=$user_id")
			    ->order("scwz_date desc")
			    ->limit(($page-1)*$pagesize,$pagesize)
			    ->select();
    
        foreach ($article as $key => $value) {
                    
            $article[$key]['talk_count']= sizeof(M('talk')->where('talk_article_id='.$value['article_id'])->select());
        }              
		echo json_encode(array('code'=>1,'article'=>$article));
		exit();

	}

	//我收藏的大师
	public function collection_master(){
		$user_id = I("user_id");
		$page = I('page');
        $pagesize = I('pagesize');
        if($page<=1){
            $page = 1;
        }
        if($pagesize<=0){
            $pagesize = 10;
        }
		$master=M("scds")
			   ->field('user_id,user_pic,user_pct,user_sex,user_content,user_content_pic,user_username,user_online,user_broken')
			   ->join("d_user on d_scds.scds_user_id=d_user.user_id")
			   ->where("scds_user_id2=".$user_id)
			   ->order("scds_date desc")
			   ->limit(($page-1)*$pagesize,$pagesize)
			   ->select();
		foreach($master as $key=>$val){
			$collection=M("scds")->where("scds_user_id=".$val["user_id"]." and scds_user_id2=".$user_id)->find();
			if(!empty($collection)){
				$master[$key]["is_collection"]=1;
			}else{
				$master[$key]["is_collection"]=0;
			}
		}
		echo json_encode(array('code'=>1,'master'=>$master));
		exit();
	}

	//我的
	public function myself(){
		$user_id = I("user_id");
		$info = M("user")
			  ->field("user_pic,user_username,user_sex,user_money")
			  ->where("user_id=".$user_id)
              ->find();
		$info["master_count"] = M("scds")->where("scds_user_id2=$user_id")->count();
		$info["article_count"] = M("scwz")->where("scwz_user_id2=$user_id")->count();
		echo json_encode(array('code'=>1,'info'=>$info));
		exit();
	}

    // save users's chat records
    public function addChatRecords(){

        $chat_user_id = I('chat_user_id');
        $chat_user_id2 = I('chat_user_id2');
        $chat_content = I('chat_content');
        $chat_time = date('Y-m-d H:i:s');

        if($chat_user_id == '' || $chat_user_id2 == '' || $chat_content == '') {
            echo json_encode(array('code'=>2));
            exit;
        }

        $chat = M('chat');
        $data['chat_user_id'] = $chat_user_id;
        $data['chat_user_id2'] = $chat_user_id2;
        $data['chat_content'] = $chat_content;
        $data['chat_time'] = $chat_time;

         if ($chat->add($data)) {
            echo json_encode(array('code'=>1));
            exit();
        } else {
            echo json_encode(array('code'=>0));
            exit();
        }
        
    }
    // buy master's service project 
    public function buyService(){

        $service_user_id1 = I('service_user_id');
        $service_user_id2 = I('service_user_id2');
        $project_id = I('project_id');
        $service_money = I('service_money');
        $payment = I('payment');
        $user_service = $user['user_money'];

        if($service_user_id1 == '' || $service_user_id2 == '' || $service_money == '' || $project_id == ''|| $payment == ''){ 
            echo json_encode(array('code'=>2));
            exit;
        }

        $this->choicePayment($service_user_id1,$service_user_id2,$service_money,$project_id,$payment,$payment);  

    }

    private function choicePayment($service_user_id1,$service_user_id2,$service_money,$project_id,$payment){

        switch ($payment) {

            case 'balance':

                if ($this->hasEnoughBalance($service_user_id1,$service_user_id2,$service_money)) { 

                    $this->balanceOperation($service_user_id1,$service_user_id2,$service_money,$project_id);            
                    echo json_encode(array('code'=>1));
                    exit;

                }

                echo json_encode(array('code'=>0));// 金额不够
                
                break;
            
            default:
                
                break;
        }
        exit;

    }
    private function balanceOperation($service_user_id1,$service_user_id2,$service_money,$project_id){
  
        $user = M('user');

        $userMaster = $this->getUserBalance($service_user_id1);
        $balanceMaster = $userMaster['user_money'];       

        $data['user_money'] = $balanceMaster+$service_money;            
        $data['total_money'] = $userMaster['total_money']+$service_money;           
        $user->where('user_id='.$service_user_id1)->save($data);

        $userCustomer = $this->getUserBalance($service_user_id2);
        $balanceCustomer = $userCustomer['user_money'];
        $data2['user_money'] = $balanceCustomer-$service_money;
        $data2['total_consume'] = $userCustomer['total_consume']+$service_money;
        $user->where('user_id='.$service_user_id2)->save($data2); 
        
        $this->manageOrder($service_user_id1,$service_user_id2,$service_money,$project_id);
        
    }
    
    private function getUserBalance($user_id){

        $user =  M('user')->field('user_money,total_money,total_consume')->where('user_id='.$user_id)->find();
        return $user;       
    }
    
    private function checkBalance($user_id,$service_money){
        $user = M('user')->field('user_money')->where('user_id='.$user_id)->select();  
        $user_money = $user[0]['user_money'];        
        return $user_money;
    }

    // check balance is or not enough 
    private function hasEnoughBalance($service_user_id1,$user_id,$service_money){
        $user_money = $this->checkBalance($user_id,$service_money);

        return ($user_money >= $service_money) ? true : false;
    }

    private function manageOrder($order_user_id,$order_user_id2,$order_balance,$project_id){

        $order = M('order');
        $order -> create();
        $data['order_user_id'] = $order_user_id;
        $data['order_user_id2'] = $order_user_id2;
        $data['order_balance'] = $order_balance;
        $data['project_id'] = $project_id; 


        $data['order_id'] = $yCode[intval(date('Y')) - 2011] . strtoupper(dechex(date('m'))) . date('d') . substr(time(), -5) . substr(microtime(), 2, 5) . sprintf('%02d', rand(0, 99));

        $data['order_date'] = date('Y-m-d H:i:s');
        $order -> add($data);

    }

}