<?php
namespace App\Controller;
use Think\Controller;
import("Org.Util.Easemob");
class IndexController extends Controller {

    var $options = array(

        'client_id' => 'YXA6NOdKUNteEeWN2q0JyRreYg',
        'client_secret' => 'YXA6ulVy04afB-feVWZW4CzMdDekKjo',
        'org_name' => '2011-fangzhou',
        'app_name' => 'forecast',

    );

	public function register() {

        $user_tel = I('user_tel');
        $user_pwd = I('user_pwd');
        if ($user_tel == '' || $user_pwd == '') {
        	echo json_encode(array('code'=>2)); //提交信息不完整
        	exit;
        }
        $u = M('user');
        $user = $u->where("user_tel='".$user_tel."'")->select();
        if ($user) {
            echo json_encode(array('code'=>3)); //此号码已经注册过
            exit;
        }
        $data['user_tel'] = $user_tel;
        $data['user_pwd'] = $user_pwd;

        $h = new \Org\Util\Easemob($this->options);

        if ($user_id = $u->add($data)) {

            $h->createUser($user_tel,$user_pwd);

        	echo json_encode(array('code'=>1,'user_id'=>$user_id)); //成功
        } else {
            echo json_encode(array('code'=>0)); //失败
        }
    }
    public function login() {

        $user_tel = I('user_tel');
        $user_pwd = I('user_pwd');

        if ($user_tel == '' || $user_pwd == '') {
        	echo json_encode(array('code'=>2)); //提交信息不完整
        	exit;
        }

        // $user = M('user')->where("user_tel='".$user_tel."' and user_pwd='".$user_pwd."'")->find();

        // if ($user) {
        // 	echo json_encode(array('code'=>1,'user'=>$user)); //成功
        // } else {
        // 	echo json_encode(array('code'=>0)); //失败
        // }

        $this->choiceLoginMethod($user_tel,$user_pwd,$token,'app');
    }



    private function choiceLoginMethod($user_tel,$user_pwd,$token,$loginMethod){

        switch ($loginMethod) {
            case 'app':
                $user = $this->findUserByUserTelAndPwd($user_tel,$user_pwd);
                if ($user) {
                    echo json_encode(array('code'=>1,'user'=>$user)); //成功
                } else {
                    echo json_encode(array('code'=>0)); //失败
                }
                break;
 
            case 'wx':
                $user = $this->findUserByToken($token);
                if ($user) {
                    echo json_encode(array('code'=>1,'user'=>$user)); //成功
                } else {

                    $user = M('user')->create();

                    echo json_encode(array('code'=>0)); //失败
                    $user->add($data)
                }
                break;
            
            default:
                $user = $this->findUserByUserTelAndPwd($user_tel,$user_pwd);
                break;
        }
    }

    public function hasUserRecord($user_tel,$user_pwd,$token){

        return (!empty($this->findUserByToken($token)))?true : false;
    }

    private function findUserByUserTelAndPwd($user_tel,$user_pwd){

        $user = M('user')->where("user_tel='".$user_tel."' and user_pwd='".$user_pwd."'")->find();
        return $user;  
    }

    private function findUserByToken($token){

        $user = M('user')->where('token=$token')->find();
        return $user;

    }
    
    public function updatepwd() {
    	$user_id = I('user_id');
        $user_pwd = I('user_pwd');
        if ($user_id == '' || $user_pwd == '') {
        	echo json_encode(array('code'=>2)); //提交信息不完整
        	exit;
        }
        $data['user_pwd'] = $user_pwd;
        $true = M('user')->where("user_id=".$user_id)->save($data);
        if ($true !== false) {
        	echo json_encode(array('code'=>1));
        } else {
        	echo json_encode(array('code'=>0));
        }
    }
    public function forget() {
    	$user_tel = I('user_tel');
        $user_pwd = I('user_pwd');
        if ($user_tel == '' || $user_pwd == '') {
        	echo json_encode(array('code'=>2)); //提交信息不完整
        	exit;
        }
        $u = M('user');
        $user = $u->where("user_tel='".$user_tel."'")->find();
        if ($user) {
        	$data['user_pwd'] = $user_pwd;
            $true = $u->where("user_id=".$user['user_id'])->save($data);
            if ($true !== false) {
            	$user['user_pwd'] = $user_pwd;
            	echo json_encode(array('code'=>1,'user'=>$user));
            } else {
            	echo json_encode(array('code'=>0));
            }
        } else {
        	echo json_encode(array('code'=>3)); //该手机还未注册
        }
    }
    public function info() {
    	$user_id = I('user_id');
    	$user_username = I('user_username');
    	$user_sex = I('user_sex');
    	if ($user_id == '' || $user_username == '' || $user_sex == '') {
        	echo json_encode(array('code'=>2)); //提交信息不完整
        	exit;
        }
        $data['user_username'] = $user_username;
        $data['user_sex'] = $user_sex;
        if (M('user')->where("user_id=$user_id")->save($data)) {
        	echo json_encode(array('code'=>1)); //修改成功
        } else {
        	echo json_encode(array('code'=>0)); //修改失败
        }
    }
    public function picture() {
    	$user_id = I('user_id');
    	$user_username = I('user_username');
    	$user_sex = I('user_sex');
    	if ($user_id == '' || $user_username == '' || $user_sex == '') {
        	echo json_encode(array('code'=>2)); //提交信息不完整
        	exit;
        }
        $data['user_username'] = $user_username;
        $data['user_sex'] = $user_sex;
        if (M('user')->where("user_id=$user_id")->save($data)) {
        	echo json_encode(array('code'=>1)); //修改成功
        } else {
        	echo json_encode(array('code'=>0)); //修改失败
        }
    }
    public function uplodeimg() {
    	$user_id = I('user_id');
		$img = $_REQUEST['img'];
		$extension = $_REQUEST['extension'];
		$img = base64_decode(str_replace(' ','+',$img));
		if ($user_id == '' || $img == '' || $extension == '') {
			  echo json_encode(array('code'=>2)); //提交信息不完整
			  exit;			
		}
        $filePath = './uploads/'.date('Ym').'/';
        $filePath2 = '/uploads/'.date('Ym').'/';
        if (!is_dir($filePath)){
           $this->create_folders($filePath);
        }
		$img_name = time().'_APP.'.$extension;
		$filename = $filePath.$img_name;
		$fp = fopen("$filename", "w+");
		if (!is_writable($filename)){
			  echo json_encode(array('code'=>3)); //异常1
			  exit();
		}
		fclose($fp);
		if (is_writable($filename) == false) {
			echo json_encode(array('code'=>4)); //异常2
			exit();
		}
		$r = file_put_contents ($filename, $img);
		if ($r) {
			$data['user_pic'] = $filePath2.$img_name;
			if (M('user')->where("user_id=$user_id")->save($data)) {
	        	echo json_encode(array('code'=>1,'path'=>$filePath2.$img_name)); //上传成功
	        } else {
	        	echo json_encode(array('code'=>0)); //上传失败
	        }
		} else {
			echo json_encode(array('code'=>0));
		}
	}
	public function create_folders($dir) {
	    return is_dir($dir) or ($this->create_folders(dirname($dir)) and mkdir($dir, 0777));
	}
	public function feedback() {
		$user_id = I('user_id');
		$feedback_content = I('content');
		if ($user_id == '' || $feedback_content == '') {
			echo json_encode(array('code'=>2)); //提交信息不完整
			exit;			
		}
		$data['feedback_user_id'] = $user_id;
		$data['feedback_content'] = $feedback_content;
		$data['feedback_date'] = time();
		if (M('feedback')->add($data)) {
			echo json_encode(array('code'=>1)); //反馈成功
		} else {
			echo json_encode(array('code'=>0)); //反馈失败
		}
	}
    public function talk_add() {

        // var_dump("expression");
        // die();
        $user_id = I('user_id');
        $article_id = I('article_id');
        $talk_content = I('content');

        if ($user_id == '' || $article_id == '' || $talk_content == '') {
            echo json_encode(array('code'=>2)); //提交信息不完整
            exit;           
        }
        $data['talk_user_id'] = $user_id;
        $data['talk_article_id'] = $article_id;
        $data['talk_content'] = $talk_content;
        $data['talk_date'] = time();
        if (M('talk')->add($data)) {
            echo json_encode(array('code'=>1)); //评论成功
        } else {
            echo json_encode(array('code'=>0)); //评论失败
        }
    }
    public function layer() {
        $layer = M('layer')->field('layer_link,layer_pic')->order('layer_top desc,layer_topdate desc')->select();
        if ($layer) {
            echo json_encode(array('code'=>1,'layer'=>$layer));
        } else {
            echo json_encode(array('code'=>0));
        }
    }
    public function category() {
        $category = M('category')->field('category_id,category_name,category_pic2')->order('category_top desc,category_topdate desc')->select();
        if ($category) {
            echo json_encode(array('code'=>1,'category'=>$category));
        } else {
            echo json_encode(array('code'=>0));
        }
    }
    public function category_list() {
        $category_id = I('category_id');
        if ($category_id == '') {
            echo json_encode(array('code'=>2)); //提交信息不完整
            exit;           
        }
        $category_pic = M('category')->field('category_pic')->where("category_id=$category_id")->find();
        $u = M();
        $sql = "select user_id,user_pic,user_pct,user_sex,user_content,user_content_pic,user_username,user_online,user_broken from d_user where user_speciality=$category_id or user_speciality like'%,$category_id,%' order by user_top desc,user_topdate desc";
        $category_list = $u->query($sql);
        if ($category_list) {
            echo json_encode(array('code'=>1,'category_list'=>$category_list,'category_pic'=>$category_pic['category_pic']));
        } else {
            echo json_encode(array('code'=>0));
        }
    }
    public function hots() {
		$user_id=I("user_id");
        $user = M('user')->field('user_id,user_pic,user_pct,user_sex,user_content,user_content_pic,user_username,user_online,user_broken')->where("user_identity_id=1")->order('user_top desc,user_topdate desc')->select();
		foreach($user as $key=>$val){

            if ($user_id) {                
                $collection=M("scds")->where("scds_user_id=".$val["user_id"]." and scds_user_id2=".$user_id)->find();
                if(!empty($collection)){
                    $user[$key]["is_collection"]=1;
                }else{
                    $user[$key]["is_collection"]=0;
                }
            }else{
                 $user[$key]["is_collection"]=0;
            }
			
		}
        if ($user) {
            echo json_encode(array('code'=>1,'user'=>$user));
        } else {
            echo json_encode(array('code'=>0));
        }
    }
    public function show() {

        $user_id = I('user_id');
        if ($user_id == '') {
            echo json_encode(array('code'=>2)); //提交信息不完整
            exit;
        }
        $total = M('project')->where("project_user_id=$user_id")->count();
        $page = I('page');
        $pagesize = I('pagesize');
        $total_page = ceil($total/$pagesize);
        if ($page <= 0) {
            $page = 1;
        }
        if ($pagesize <= 0) {
            $pagesize = 20;
        }
        $project = M('project')->field('project_id,project_title,project_price,project_content')->where("project_user_id=$user_id")->limit(($page - 1) * $pagesize, $pagesize)->order('project_id desc')->select();
        if ($project) {
            echo json_encode(array('code'=>1,'project'=>$project,'total'=>$total,'total_page'=>$total_page));
        } else {
            echo json_encode(array('code'=>0));
        }
    }
    public function article() {

        $user_id = I('user_id');
        if ($user_id == '') {
            echo json_encode(array('code'=>2)); //提交信息不完整
            exit;
        }
        $total = M('article')->where("article_user_id=$user_id")->count();

        $page = I('page');
        $pagesize = I('pagesize');
        $total_page = ceil($total/$pagesize);
        if ($page <= 0) {
            $page = 1;
        }
        if ($pagesize <= 0) {
            $pagesize = 20;
        }
        $article = M('article')->field('article_id,article_title,article_pic,article_content,article_date')->where("article_user_id=$user_id and article_status=1")->limit(($page - 1) * $pagesize, $pagesize)->order('article_id desc')->select();

		foreach($article as $key=>$val){
			$article[$key]["count"]=M("talk")->where("talk_article_id=".$val["article_id"])->count();
		}
		$author = M("user")->where("user_id=".$user_id)->getField("user_username");
		
        if ($article) {
            echo json_encode(array('code'=>1,'article'=>$article,'total'=>$total,'total_page'=>$total_page));
        } else {
            echo json_encode(array('code'=>0));
        }
    }

	//首页搜索预测师
	public function search_master(){
		$keywards = I("keywards");




	}
	//显示预测师的第一条评论
	public function one_comment(){
		$user_id = I("user_id");
		$info = M("ask")
			  ->field("user_pic,user_username,ask_value,ask_content,ask_date")
			  ->join("d_user on d_user.user_id=d_ask.ask_user_id2")
			  ->where("ask_user_id=$user_id")
			  ->order("ask_date desc")
			  ->limit(1)
			  ->find();
		$count=M("ask")->where("ask_user_id=$user_id")->count();
		echo json_encode(array("code"=>1,"info"=>$info,"count"=>$count));

	}
}