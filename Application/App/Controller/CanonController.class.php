<?php
namespace App\Controller;
use Think\Controller;
class CanonController extends Controller {
	//宝典
	public function canon_index(){
		$type = M("type")->field('type_id,type_name,type_pic')->order("type_top desc,type_topdate desc")->select();
		$page = I('page');
        $pagesize = I('pagesize');
        if($page<=1){
            $page = 1;
        }
        if($pagesize<=0){
            $pagesize = 10;
        }
		$article = M('article')
        ->field('article_id,article_title,article_content,article_pic,article_status,article_date,user_username')
        ->join('d_user on d_article.article_user_id=d_user.user_id')
        ->where('article_status=1')
        ->order('article_date desc')
		->limit(($page-1)*$pagesize,$pagesize)
        ->select();
		foreach($article as $k=>$v){
			$article[$k]["comment_count"] = M("talk")->where("talk_article_id=".$v["article_id"])->count();
		}
		echo json_encode(array('code'=>1,"type"=>$type,"article"=>$article));

	}
	//点击某一个文章分类进入文章列表
	public function type_articlelist(){
		$type_id = I("type_id");
		$page = I('page');
        $pagesize = I('pagesize');
        if($page<=1){
            $page = 1;
        }
        if($pagesize<=0){
            $pagesize = 10;
        }
		$article = M('article')
        ->field('article_id,article_type_id,article_title,article_content,article_pic,article_status,article_date,user_username')
        ->join('d_user on d_article.article_user_id=d_user.user_id')
		->where("article_type_id=".$type_id)
        ->order('article_date desc,article_topdate desc')
		->limit(($page-1)*$pagesize,$pagesize)
        ->select();
		//echo $article->getLastSql();die;
		foreach($article as $k=>$v){
			$article[$k]["comment_count"] = M("talk")->where("talk_article_id=".$v["article_id"])->count();
		}
		echo json_encode(array('code'=>1,"article"=>$article));
	}

	//文章详情
	public function article_detail(){


		$article_id = I("article_id");
		$user_id=I("user_id");

		$detail = M("article")
		    ->field("article_user_id,article_type_id,article_pic,article_title,article_content,article_date,user_username,user_pic,type_name")
		    ->join("d_user on d_article.article_user_id=d_user.user_id")
            ->join("d_type on d_article.article_type_id=d_type.type_id")
		    ->where("article_id=$article_id")
		    ->find();

		if ($user_id) {		

			$collection = M("scwz")->where("scwz_article_id=".$article_id." and scwz_user_id2=".$user_id)->find();
			if(!empty($collection)){
				$is_collection=1;
			}else{
				$is_collection=0;
			}
			//echo $is_collection;die;
			$praise = M("praise")->where("praise_article_id=".$article_id." and praise_user_id=".$user_id)->find();
			if(!empty($praise)){
				$is_praise=1;
			}else{
				$is_praise=0;
			}
			
			echo json_encode(array('code'=>1,"detail"=>$detail,'is_collection'=>$is_collection,'is_praise'=>$is_praise));
			exit();

		}

		echo json_encode(array('code'=>1,"detail"=>$detail,'is_collection'=>0,'is_praise'=>0));
			exit();
	}
	//某个文章的评论
	public function article_comment(){
		$article_id = I("article_id");
		$user_id = I("user_id");
		$page = I('page');
        $pagesize = I('pagesize');
        if($page<=1){
            $page = 1;
        }
        if($pagesize<=0){
            $pagesize = 10;
        }
		$comment = M("talk")
			     ->field("user_pic,user_username,talk_user_id,talk_content,talk_date,talk_article_id")
			     ->join("d_user on d_talk.talk_user_id=d_user.user_id")
			     ->where("talk_article_id=".$article_id)
			     ->order("talk_date")
			     ->limit(($page-1)*$pagesize,$pagesize)
			     ->select();
		$praise_count = M("praise")->where("praise_article_id=".$article_id)->count();
		$comment_count = M("talk")->where("talk_article_id=".$article_id)->count();
		$praise=M("praise")->where("praise_article_id=".$article_id." and praise_user_id=".$user_id)->find();
		if($praise){
			$is_praise=1;
		}else{
			$is_praise=0;
		}
		echo json_encode(array('code'=>1,"comment"=>$comment,"praise_count"=>$praise_count,"comment_count"=>$comment_count,"is_praise"=>$is_praise));
		exit();
	}

} 