<?php
namespace App\Controller;
use Think\Controller;
class MainController extends Controller {
	public function category_list() {
        $category = M('category')->field('category_id,category_name')->order('category_top desc,category_topdate desc')->select();
        if ($category) {
            echo json_encode(array('code'=>1,'category'=>$category));
        } else {
            echo json_encode(array('code'=>0));
        }
    }
    public function project_add() {
        $user_id = I('user_id');
        $project_title = I('project_title');
        $project_price = I('project_price');
        $category_id = I('category_id');
        $project_content = I('project_content');
        if ($user_id == '' || $category_id == '' || $project_title == '' || $project_price == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        $data['project_user_id'] = $user_id;
        $data['project_category_id'] = $category_id;
        $data['project_title'] = $project_title;
        $data['project_price'] = $project_price;
        $data['project_content'] = $project_content;
        $data['project_date'] = time();
        if (M('project')->add($data)) {
            echo json_encode(array('code'=>1));
        } else {
            echo json_encode(array('code'=>0));
        }
    }
    public function project_list() {
        $user_id = I('user_id');
        if ($user_id == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        $project = M('project')
			       ->field('project_id,project_title,project_price,project_content,project_category_id,category_name')
			       ->join("d_category on category_id=d_project.project_category_id")
			       ->order('project_id desc')->select();
        if ($project) {
            echo json_encode(array('code'=>1,'project'=>$project));
        } else {
            echo json_encode(array('code'=>0));
        }
    }
    public function project_edit() {
        $project_id = I('project_id');
        $project_title = I('project_title');
        $project_price = I('project_price');
        $category_id = I('category_id');
        $project_content = I('project_content');
        if ($project_id == '' || $category_id == '' || $project_title == '' || $project_price == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        $data['project_category_id'] = $category_id;
        $data['project_title'] = $project_title;
        $data['project_price'] = $project_price;
        $data['project_content'] = $project_content;
        if (M('project')->where("project_id=$project_id")->save($data) !== false) {
            echo json_encode(array('code'=>1));
        } else {
            echo json_encode(array('code'=>0));
        }
    }
    public function project_del() {
        $project_id = I('project_id');
        if ($project_id == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        if (M('project')->where("project_id=$project_id")->delete()) {
            echo json_encode(array('code'=>1));
        } else {
            echo json_encode(array('code'=>0));
        }
    }
    public function type_list() {
        $type = M('type')->field('type_id,type_name')->order('type_top desc,type_topdate desc')->select();
        if ($type) {
            echo json_encode(array('code'=>1,'type'=>$type));
        } else {
            echo json_encode(array('code'=>0));
        }
    }
    public function article_add() {
        $user_id = I('user_id');
        $img = $_REQUEST['img'];
		$extension = $_REQUEST['extension'];
		if($img!="" && $extension!=""){
			$img = base64_decode(str_replace(' ','+',$img));
			$filePath = './uploads/'.date('Ym').'/';
			$filePath2 = '/uploads/'.date('Ym').'/';
			if (!is_dir($filePath)){
			   $this->create_folders($filePath);
			}
			$img_name = time().'_APP.'.$extension;
			$filename = $filePath.$img_name;
			$fp = fopen("$filename", "w+");
			if (!is_writable($filename)){
				  echo json_encode(array('code'=>3)); //异常1
				  exit();
			}
			fclose($fp);
			if (is_writable($filename) == false) {
				echo json_encode(array('code'=>4)); //异常2
				exit();
			}
			$r = file_put_contents ($filename, $img);
			if ($r) {
				$data['article_pic'] = $filePath2.$img_name;
			}
		}
        $type_id = I('type_id');
        $article_title = I('article_title');
        $article_content = I('article_content');
        if ($user_id == "" || $type_id == "" || $article_title == "" || $img == "" || $extension == "") {
            echo json_encode(array('code'=>2));
            exit;
        }
            $data['article_user_id'] = $user_id;
            $data['article_type_id'] = $type_id;
            $data['article_title'] = $article_title;
            $data['article_content'] = $article_content;
            $data['article_date'] = time();
            $data['article_topdate'] = time();
            if (M('article')->add($data)) {
                echo json_encode(array('code'=>1));
            } else {
                echo json_encode(array('code'=>0));
            }
    }
    public function create_folders($dir) {
        return is_dir($dir) or ($this->create_folders(dirname($dir)) and mkdir($dir, 0777));
    }
    public function article_list() {
        $user_id = I('user_id');
        if ($user_id == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        $article = M('article')
        ->field('article_id,article_title,article_content,article_pic,article_status,article_date,user_username')
        ->join('d_user on d_article.article_user_id=d_user.user_id')
        ->where("article_user_id=$user_id")
        ->order('article_id desc')
        ->select();
        if ($article) {
            foreach ($article as $key => $value) {
                $article[$key]['num'] = M('talk')->field('talk_id')->where("talk_article_id=".$value['article_id'])->count();
            }
            echo json_encode(array('code'=>1,'article'=>$article));
        } else {
            echo json_encode(array('code'=>0));
        }
    }
    public function article_edit() {
        $article_id = I('article_id');
        $img = $_REQUEST['img'];
        $extension = $_REQUEST['extension'];
        $type_id = I('type_id');
        $article_title = I('article_title');
        $article_content = I('article_content');
        if ($article_id == '' || $type_id == '' || $article_title == '' || $img == '' || $extension == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        $img = base64_decode(str_replace(' ','+',$img));
        $filePath = './uploads/'.date('Ym').'/';
        $filePath2 = '/uploads/'.date('Ym').'/';
        if (!is_dir($filePath)){
           $this->create_folders($filePath);
        }
        $img_name = time().'_APP.'.$extension;
        $filename = $filePath.$img_name;
        $fp = fopen("$filename", "w+");
        if (!is_writable($filename)){
              echo json_encode(array('code'=>3)); //异常1
              exit();
        }
        fclose($fp);
        if (is_writable($filename) == false) {
            echo json_encode(array('code'=>4)); //异常2
            exit();
        }
        $r = file_put_contents ($filename, $img);
        if ($r) {
            $data['article_pic'] = $filePath2.$img_name;
            $data['article_type_id'] = $type_id;
            $data['article_title'] = $article_title;
            $data['article_content'] = $article_content;
            if (M('article')->where("article_id=$article_id")->save($data)) {
                echo json_encode(array('code'=>1));
            } else {
                echo json_encode(array('code'=>0));
            }
        } else {
            echo json_encode(array('code'=>0));
        }
    }
    public function article_del() {
        $article_id = I('article_id');
        if ($article_id == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        if (M('article')->where("article_id=$article_id")->delete()) {
            echo json_encode(array('code'=>1));
        } else {
            echo json_encode(array('code'=>0));
        }
    }
    public function article_show() {
        $article_id = I('article_id');
        $user_id = I('user_id');
        if ($article_id == '' || $user_id == '') {
            echo json_encode(array('code'=>2));
            exit;
        }
        $article = M('article')
        ->field('article_title,article_content,article_pic,article_date,article_type_id,type_name')
        ->join('d_type on d_article.article_type_id=d_type.type_id')
        ->where("article_id=$article_id")
        ->find();
        $user = M('user')->field('user_username,user_pic')->where("user_id=$user_id")->find();
        $talk = M('talk')
        ->field('talk_user_id,talk_content,talk_date,user_username,user_pic')
        ->join('d_user on d_talk.talk_user_id=d_user.user_id')
        ->where("talk_article_id=$article_id")
        ->select();
		$praise=M("praise")
			  ->where("praise_article_id=$article_id and praise_user_id=$user_id")
			  ->find();
		if(!empty($praise)){
			$is_praise=1;
		}else{
			$is_praise=0;
		}
		$collection=M("scwz")
		     ->where("scwz_article_id=$article_id and scwz_user_id2=$user_id")
			 ->find();
		if(!empty($collectiom)){
			$is_collection_article=1;
		}else{
			$is_collection_article=0;
		}
		$praise_count=M("praise")
				  ->where("praise_article_id=$article_id")
				  ->count();
        echo json_encode(array('code'=>1,'article'=>$article,'user'=>$user,'talk'=>$talk,'is_praise'=>$is_praise,'is_collection_article'=>$is_collection_article,'praise_count'=>$praise_count));
    }



	//点赞 取消赞
	public function praise(){
		$user_id = I("user_id");
		$article_id = I("article_id");
		if ($article_id == '' || $user_id == '') {
            echo json_encode(array('code'=>2));
            exit();
        }
		$info=M("praise")
			  ->where("praise_article_id=$article_id and praise_user_id=$user_id")
			  ->find();
		if(empty($info)){
			$data["praise_article_id"]=$article_id;
			$data["praise_user_id"]=$user_id;
			$data["praise_date"]=time();
			$reg=M("praise")->add($data);
		}else{
			$reg=M("praise")->where("praise_article_id=$article_id and praise_user_id=$user_id")->delete();
		}
		if($reg){
			echo json_encode(array('code'=>1));
            exit();
		}else{
			echo json_encode(array('code'=>0));
            exit();
		}
	}
	//收藏文章/取消收藏
    public function collection_article(){
		$user_id = I("user_id");
		$article_id = I("article_id");
		if ($article_id == '' || $user_id == '') {
            echo json_encode(array('code'=>2));
            exit();
        }
		$info=M("scwz")
		     ->where("scwz_article_id=$article_id and scwz_user_id2=$user_id")
			 ->find();
		if(empty($info)){
			$data["scwz_article_id"]=$article_id;
			$data["scwz_user_id2"]=$user_id;
			$data["scwz_date"]=time();
			$reg=M("scwz")->add($data);
		}else{
			$reg=M("scwz")->where("scwz_article_id=$article_id and scwz_user_id2=$user_id")->delete();
		}
		if($reg){
			echo json_encode(array('code'=>1));
            exit();
		}else{
			echo json_encode(array('code'=>0));
            exit();
		}
	}
	//收藏大师/取消收藏
	public function collection_master(){
		$master_id = I("user_id");
		$user_id = I("user_id2");
		if ($master_id == '' || $user_id == '') {
            echo json_encode(array('code'=>2));
            exit();
        }
		$info=M("scds")
			 ->where("scds_user_id=$master_id and scds_user_id2=$user_id")
			 ->find();
		if(empty($info)){
			$data["scds_user_id"]=$master_id;
			$data["scds_user_id2"]=$user_id;
			$data["scds_date"]=time();
			$reg=M("scds")->add($data);
		}else{
			$reg=M("scds")->where("scds_user_id=$master_id and scds_user_id2=$user_id")->delete();
		}
		if($reg){
			echo json_encode(array('code'=>1));
            exit();
		}else{
			echo json_encode(array('code'=>0));
            exit();
		}
	}

	//大师----我的评价
	public function my_comment(){
		$user_id = I("user_id");
		$comment=M("ask")
			    ->field("ask_user_id,ask_user_id2,ask_content,ask_date,user_username,user_pic")
			    ->join("d_user on d_user.user_id=d_ask.ask_user_id2")
			    ->where("ask_user_id=$user_id")
			    ->select();
		if(!empty($comment)){
			echo json_encode(array('code'=>1,'comment_list'=>$comment));
            exit();
		}else{
			echo json_encode(array('code'=>0));
            exit();
		}

	}

	//关于人人算
	public function about(){
		$content=M("about")->where("about_id=1")->getField("about_content");
		echo "<meta charset='utf-8'><meta name='format-detection' content='telephone=no'/><meta name='viewport' content='user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height'/><div id='xiangqin' style='width:100%;font-size:1em'>".htmlspecialchars_decode($content)."</div> <style> #xiangqin img{width:100%;}</style>";die;
		exit();
	}

	//人人算协议
	public function agreement(){
		$content=M("about")->where("about_id=2")->getField("about_content");
		echo "<meta charset='utf-8'><meta name='format-detection' content='telephone=no'/><meta name='viewport' content='user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height'/><div id='xiangqin' style='width:100%;font-size:1em'>".htmlspecialchars_decode($content)."</div> <style> #xiangqin img{width:100%;}</style>";die;
		exit();
	}

	//申请提现
	public function withdrawals(){
		$data["w_user_id"] = I("user_id");
		$data["w_money"] = I("money");
		$data["w_date"] = time();
		$reg=M("withdrawals")->add($data);
		if($reg){
			echo json_encode(array('code'=>1));
            exit();
		}else{
			echo json_encode(array('code'=>0));
            exit();
		}
	}
}