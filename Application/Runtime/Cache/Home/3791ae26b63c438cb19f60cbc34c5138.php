<?php if (!defined('THINK_PATH')) exit();?> <!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>人人算PC端管理</title>

		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        
		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="/Public/assets/css/bootstrap.css" />
		<link rel="stylesheet" href="/Public/assets/css/font-awesome.css" />

		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<link rel="stylesheet" href="/Public/assets/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="/Public/assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<link rel="stylesheet" href="/Public/assets/css/ace-skins.css" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="/Public/assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="/Public/assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="/Public/assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="/Public/assets/js/html5shiv.js"></script>
		<script src="/Public/assets/js/respond.js"></script>
		<![endif]-->
		
<link href="/Public/assets/css/select2.css" rel="stylesheet">

		
<style type='text/css'>
.UploadBox{overflow:hidden}
.UploadValue{width:260px;float:left;margin-right:4px;text-indent:6px}
.UploadConfirm{position:absolute;top:0;left:0;height:35px;filter:alpha(opacity:0);opacity:0;width:337px}
</style>

	</head>

	<body class="skin-2">
		<!-- #section:basics/navbar.layout -->
		<div id="navbar" class="navbar navbar-default">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>

			<div class="navbar-container" id="navbar-container">
				<!-- #section:basics/sidebar.mobile.toggle -->
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<!-- /section:basics/sidebar.mobile.toggle -->
				<div class="navbar-header pull-left">
					<!-- #section:basics/navbar.layout.brand -->
					<a href="#" class="navbar-brand">
						<small style='font-size:23px'>
							人人算PC端管理
						</small>
					</a>

					<!-- /section:basics/navbar.layout.brand -->

					<!-- #section:basics/navbar.toggle -->

					<!-- /section:basics/navbar.toggle -->
				</div>

				<!-- #section:basics/navbar.dropdown -->
				<div class="navbar-buttons navbar-header pull-right" role="navigation">
				    <ul class="nav ace-nav">
						<li class="purple no-border margin-1 light-pink">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-envelope icon-animated-vertical pink"></i>
								<span class="badge badge-success">8</span>
							</a>

							<ul class="dropdown-menu-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="ace-icon fa fa-exclamation-triangle"></i>
									8 条最新消息
								</li>

								<li class="dropdown-content ace-scroll" style="position: relative;"><div class="scroll-track" style="display: none;"><div class="scroll-bar"></div></div><div class="scroll-content" style="max-height: 200px;">
									<ul class="dropdown-menu dropdown-navbar">
										<li>
											<a href="#" class="clearfix">
												<img src="/Public/assets/avatars/avatar.png" class="msg-photo" alt="Alex's Avatar">
												<span class="msg-body">
													<span class="msg-title">
														<span class="blue">张大师:</span>
														算卦得10000000块钱 ... ...
													</span>

													<span class="msg-time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span>2016.12.23 18:32</span>
													</span>
												</span>
											</a>
										</li>

										<li>
											<a href="#" class="clearfix">
												<img src="/Public/assets/avatars/avatar3.png" class="msg-photo" alt="Susan's Avatar">
												<span class="msg-body">
													<span class="msg-title">
														<span class="blue">纯元皇后:</span>
														申请提现280元 ...
													</span>

													<span class="msg-time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span>2016.12.23 18:32</span>
													</span>
												</span>
											</a>
										</li>

										<li>
											<a href="#" class="clearfix">
												<img src="/Public/assets/avatars/avatar4.png" class="msg-photo" alt="Bob's Avatar">
												<span class="msg-body">
													<span class="msg-title">
														<span class="blue">张怡:</span>
														刚刚充值1000元 ...
													</span>

													<span class="msg-time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span>2016.12.23 18:32</span>
													</span>
												</span>
											</a>
										</li>

										<li>
											<a href="#" class="clearfix">
												<img src="/Public/assets/avatars/avatar2.png" class="msg-photo" alt="Kate's Avatar">
												<span class="msg-body">
													<span class="msg-title">
														<span class="blue">刘亚玲:</span>
														看奇门遁甲骗人被骂 ...
													</span>

													<span class="msg-time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span>2016.12.23 18:32</span>
													</span>
												</span>
											</a>
										</li>

										<li>
											<a href="#" class="clearfix">
												<img src="/Public/assets/avatars/avatar5.png" class="msg-photo" alt="Fred's Avatar">
												<span class="msg-body">
													<span class="msg-title">
														<span class="blue">朱元璋:</span>
														朱元璋发布了最新的文章  ... ... ...
													</span>

													<span class="msg-time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span>2016.12.23 18:32</span>
													</span>
												</span>
											</a>
										</li>
									</ul>
								</div></li>

								<li class="dropdown-footer">
									<a href="#">
										查看更多消息
										<i class="ace-icon fa fa-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<!-- #section:basics/navbar.user_menu -->
						<li class="light-blue no-border margin-1">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="/renren/Public/assets/avatars/user.jpg" alt="admin's photo">
								<span class="user-info">
									<small>欢迎</small>
									admin
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="profile.html">
										<i class="ace-icon fa fa-key"></i>
										修改密码
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="#">
										<i class="ace-icon fa fa-power-off"></i>
										退出登录
									</a>
								</li>
							</ul>
						</li>

						<!-- /section:basics/navbar.user_menu -->
					</ul>
				</div>

				<!-- /section:basics/navbar.dropdown -->
			</div><!-- /.navbar-container -->
		</div>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar -->
			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<button class="btn btn-success">
							<i class="ace-icon fa fa-signal"></i>
						</button>

						<button class="btn btn-info">
							<i class="ace-icon fa fa-pencil"></i>
						</button>

						<!-- #section:basics/sidebar.layout.shortcuts -->
						<button class="btn btn-warning">
							<i class="ace-icon fa fa-users"></i>
						</button>

						<button class="btn btn-danger">
							<i class="ace-icon fa fa-cogs"></i>
						</button>

						<!-- /section:basics/sidebar.layout.shortcuts -->
					</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>
					</div>
				</div><!-- /.sidebar-shortcuts -->
				
                <ul class="nav nav-list">
	<li class="" id="receiveMenu10">
		<a href="#" class="dropdown-toggle">
			<i class="menu-icon fa fa-edit"></i>
			<span class="menu-text">主页</span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="" id="appointmentItem1">
				<a href="/index.php/Home/Login/save_password">
					<i class="menu-icon fa fa-caret-right"></i>
					修改密码
				</a>

				<b class="arrow"></b>
			</li>
			<li class="" id="appointmentItem2">
				<a href="/index.php/Home/Login/login_add">
					<i class="menu-icon fa fa-caret-right"></i>
					添加管理员
				</a>

				<b class="arrow"></b>
			</li>
			<li class="" id="appointmentItem3">
				<a href="/index.php/Home/Login/liste">
					<i class="menu-icon fa fa-caret-right"></i>
					管理员列表
				</a>

				<b class="arrow"></b>
			</li>
		</ul>
	</li>
    <li class="" id="receiveMenu1">
		<a href="#" class="dropdown-toggle">
			<i class="menu-icon fa fa-cog"></i>
			<span class="menu-text"> 系统设置 </span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="" id="appointmentItem11">
				<a href="/index.php/Home/System/category_list">
					<i class="menu-icon fa fa-caret-right"></i>
					项目分类
				</a>

				<b class="arrow"></b>
			</li>

			<li class="" id="appointmentItem12">
				<a href="/index.php/Home/System/type_list">
					<i class="menu-icon fa fa-caret-right"></i>
					宝典分类
				</a>

				<b class="arrow"></b>
			</li>
			
		</ul>
	</li>
	<li class="" id="receiveMenu2">
		<a href="#" class="dropdown-toggle">
			<i class="menu-icon fa fa-user"></i>
			<span class="menu-text"> 用户管理 </span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="" id="appointmentItem21">
				<a href="/index.php/Home/Index/users/p/1">
					<i class="menu-icon fa fa-caret-right"></i>
					大师列表
				</a>

				<b class="arrow"></b>
			</li>
			<li class="" id="appointmentItem4">
				<a href="/index.php/Home/Index/ordinary_user/p/1">
					<i class="menu-icon fa fa-caret-right"></i>
					用户列表
				</a>

				<b class="arrow"></b>
			</li>
		</ul>
	</li>

	<li class="" id="receiveMenu3">
		<a href="#" class="dropdown-toggle">
			<i class="menu-icon fa fa-book"></i>
			<span class="menu-text">文章管理</span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="" id="appointmentItem31">
				<a href="/index.php/Home/Article/article_list/p/1">
					<i class="menu-icon fa fa-caret-right"></i>
					文章列表
				</a>

				<b class="arrow"></b>
			</li>
		</ul>
	</li>

	<li class="" id="receiveMenu5">
		<a href="#" class="dropdown-toggle">
			<i class="menu-icon fa fa-order"></i>
			<span class="menu-text">订单管理</span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="" id="appointmentItem51">
				<a href="/index.php/Home/OrderManage/showAllOrder/p/1">
					<i class="menu-icon fa fa-caret-right"></i>
					订单列表
				</a>

				<b class="arrow"></b>
			</li>
		</ul>
	</li>

	<li class="" id="receiveMenu6">
		<a href="#" class="dropdown-toggle">
			<i class="menu-icon fa fa-chat"></i>
			<span class="menu-text">聊天记录管理</span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="" id="appointmentItem61">
				<a href="/index.php/Home/Chat/showAllChat/p/1">
					<i class="menu-icon fa fa-caret-right"></i>
					聊天记录列表
				</a>

				<b class="arrow"></b>
			</li>
		</ul>
	</li>


	<li class="" id="receiveMenu4">
		<a href="#" class="dropdown-toggle">
			<i class="menu-icon fa fa-edit"></i>
			<span class="menu-text">关于我们</span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="" id="appointmentItem41">
				<a href="/index.php/Home/System/about">
					<i class="menu-icon fa fa-caret-right"></i>
					关于人人算
				</a>

				<b class="arrow"></b>
			</li>
			<li class="" id="appointmentItem42">
				<a href="/index.php/Home/System/agreement">
					<i class="menu-icon fa fa-caret-right"></i>
					人人算协议
				</a>

				<b class="arrow"></b>
			</li>
		</ul>
	</li>

	

</ul>
<!--<ul class="nav nav-list">
	<li class="">
		<a href="index.html">
			<i class="menu-icon fa fa-tachometer"></i>
			<span class="menu-text"> Dashboard </span>
		</a>

		<b class="arrow"></b>
	</li>

	<li class="">
		<a href="#" class="dropdown-toggle">
			<i class="menu-icon fa fa-desktop"></i>
			<span class="menu-text">
				UI &amp; Elements
			</span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="">
				<a href="#" class="dropdown-toggle">
					<i class="menu-icon fa fa-caret-right"></i>

					Layouts
					<b class="arrow fa fa-angle-down"></b>
				</a>

				<b class="arrow"></b>

				<ul class="submenu">
					<li class="">
						<a href="top-menu.html">
							<i class="menu-icon fa fa-caret-right"></i>
							Top Menu
						</a>

						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="two-menu-1.html">
							<i class="menu-icon fa fa-caret-right"></i>
							Two Menus 1
						</a>

						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="two-menu-2.html">
							<i class="menu-icon fa fa-caret-right"></i>
							Two Menus 2
						</a>

						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="mobile-menu-1.html">
							<i class="menu-icon fa fa-caret-right"></i>
							Default Mobile Menu
						</a>

						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="mobile-menu-2.html">
							<i class="menu-icon fa fa-caret-right"></i>
							Mobile Menu 2
						</a>

						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="mobile-menu-3.html">
							<i class="menu-icon fa fa-caret-right"></i>
							Mobile Menu 3
						</a>

						<b class="arrow"></b>
					</li>
				</ul>
			</li>

			<li class="">
				<a href="typography.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Typography
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="elements.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Elements
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="buttons.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Buttons &amp; Icons
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="content-slider.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Content Sliders
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="treeview.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Treeview
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="jquery-ui.html">
					<i class="menu-icon fa fa-caret-right"></i>
					jQuery UI
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="nestable-list.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Nestable Lists
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="#" class="dropdown-toggle">
					<i class="menu-icon fa fa-caret-right"></i>

					Three Level Menu
					<b class="arrow fa fa-angle-down"></b>
				</a>

				<b class="arrow"></b>

				<ul class="submenu">
					<li class="">
						<a href="#">
							<i class="menu-icon fa fa-leaf green"></i>
							Item #1
						</a>

						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-pencil orange"></i>

							4th level
							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="">
								<a href="#">
									<i class="menu-icon fa fa-plus purple"></i>
									Add Product
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="#">
									<i class="menu-icon fa fa-eye pink"></i>
									View Products
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>
				</ul>
			</li>
		</ul>
	</li>

	<li class="">
		<a href="#" class="dropdown-toggle">
			<i class="menu-icon fa fa-list"></i>
			<span class="menu-text"> Tables </span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="">
				<a href="tables.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Simple &amp; Dynamic
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="jqgrid.html">
					<i class="menu-icon fa fa-caret-right"></i>
					jqGrid plugin
				</a>

				<b class="arrow"></b>
			</li>
		</ul>
	</li>

	<li class="">
		<a href="#" class="dropdown-toggle">
			<i class="menu-icon fa fa-pencil-square-o"></i>
			<span class="menu-text"> Forms </span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="">
				<a href="form-elements.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Form Elements
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="form-elements-2.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Form Elements 2
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="form-wizard.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Wizard &amp; Validation
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="wysiwyg.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Wysiwyg &amp; Markdown
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="dropzone.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Dropzone File Upload
				</a>

				<b class="arrow"></b>
			</li>
		</ul>
	</li>

	<li class="">
		<a href="widgets.html">
			<i class="menu-icon fa fa-list-alt"></i>
			<span class="menu-text"> Widgets </span>
		</a>

		<b class="arrow"></b>
	</li>

	<li class="">
		<a href="calendar.html">
			<i class="menu-icon fa fa-calendar"></i>

			<span class="menu-text">
				Calendar

				<span class="badge badge-transparent tooltip-error" title="2 Important Events">
					<i class="ace-icon fa fa-exclamation-triangle red bigger-130"></i>
				</span>

			</span>
		</a>

		<b class="arrow"></b>
	</li>

	<li class="">
		<a href="gallery.html">
			<i class="menu-icon fa fa-picture-o"></i>
			<span class="menu-text"> Gallery </span>
		</a>

		<b class="arrow"></b>
	</li>

	<li class="">
		<a href="#" class="dropdown-toggle">
			<i class="menu-icon fa fa-tag"></i>
			<span class="menu-text"> More Pages </span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="">
				<a href="profile.html">
					<i class="menu-icon fa fa-caret-right"></i>
					User Profile
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="inbox.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Inbox
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="pricing.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Pricing Tables
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="invoice.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Invoice
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="timeline.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Timeline
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="email.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Email Templates
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="login.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Login &amp; Register
				</a>

				<b class="arrow"></b>
			</li>
		</ul>
	</li>

	<li class="active open">
		<a href="#" class="dropdown-toggle">
			<i class="menu-icon fa fa-file-o"></i>

			<span class="menu-text">
				Other Pages

				<span class="badge badge-primary">5</span>

			</span>

			<b class="arrow fa fa-angle-down"></b>
		</a>

		<b class="arrow"></b>

		<ul class="submenu">
			<li class="">
				<a href="faq.html">
					<i class="menu-icon fa fa-caret-right"></i>
					FAQ
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="error-404.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Error 404
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="error-500.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Error 500
				</a>

				<b class="arrow"></b>
			</li>

			<li class="">
				<a href="grid.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Grid
				</a>

				<b class="arrow"></b>
			</li>

			<li class="active">
				<a href="blank.html">
					<i class="menu-icon fa fa-caret-right"></i>
					Blank Page
				</a>

				<b class="arrow"></b>
			</li>
		</ul>
	</li>
</ul>-->

				<!-- #section:basics/sidebar.layout.minimize -->
				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>

				<!-- /section:basics/sidebar.layout.minimize -->
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
				</script>
			</div>

			<!-- /section:basics/sidebar -->
			<div class="main-content">
				<div class="main-content-inner">
					<!-- #section:basics/content.breadcrumbs -->
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>
                        
<ul class="breadcrumb">
	<li>
		<i class="ace-icon fa fa-home home-icon"></i>
		<a href="#">主页</a>
	</li>
    <li>
		<a href="/index.php/Home/Index/users/p/<?php echo ($p); ?>">聊天记录列表</a>
	</li>
	<li class="active">聊天记录详情</li>
</ul><!-- /.breadcrumb -->

<!-- #section:basics/content.searchbox -->
<div class="nav-search" id="nav-search"></div><!-- /.nav-search -->


						<!-- /section:basics/content.searchbox -->
					</div>

					<!-- /section:basics/content.breadcrumbs -->
					<div class="page-content">
						<!-- #section:settings.box -->
						<div class="ace-settings-container" id="ace-settings-container">
							<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
								<i class="ace-icon fa fa-cog bigger-130"></i>
							</div>

							<div class="ace-settings-box clearfix" id="ace-settings-box">
								<div class="pull-left width-50">
									<!-- #section:settings.skins -->
									<div class="ace-settings-item">
										<div class="pull-left">
											<select id="skin-colorpicker" class="hide">
												<option data-skin="skin-2" value="#C6487E">#C6487E</option>
												<option data-skin="no-skin" value="#438EB9">#438EB9</option>
												<option data-skin="skin-1" value="#222A2D">#222A2D</option>
												<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
											</select>
										</div>
										<span>&nbsp; Choose Skin</span>
									</div>

									<!-- /section:settings.skins -->

									<!-- #section:settings.navbar -->
									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar" />
										<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
									</div>

									<!-- /section:settings.navbar -->

									<!-- #section:settings.sidebar -->
									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar" />
										<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
									</div>

									<!-- /section:settings.sidebar -->

									<!-- #section:settings.breadcrumbs -->
									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-breadcrumbs" />
										<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
									</div>

									<!-- /section:settings.breadcrumbs -->

									<!-- #section:settings.rtl -->
									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" />
										<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
									</div>

									<!-- /section:settings.rtl -->

									<!-- #section:settings.container -->
									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-add-container" />
										<label class="lbl" for="ace-settings-add-container">
											Inside
											<b>.container</b>
										</label>
									</div>

									<!-- /section:settings.container -->
								</div><!-- /.pull-left -->

								<div class="pull-left width-50">
									<!-- #section:basics/sidebar.options -->
									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" />
										<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" />
										<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" />
										<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
									</div>

									<!-- /section:basics/sidebar.options -->
								</div><!-- /.pull-left -->
							</div><!-- /.ace-settings-box -->
						</div><!-- /.ace-settings-container -->

						<!-- /section:settings.box -->
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
                                
<form action="/index.php/Home/Chat/updateChatContent/chat_id/<?php echo ($chat["id"]); ?>" method='post' id='myformedit' >
<input type="hidden" name='chat_id' id ="chat_id" value="<?php echo ($chat["id"]); ?>">
<input type="hidden" name='p' value="<?php echo ($p); ?>">
<input type="hidden" name='speciality' id='speciality'>

<div class="row" style="margin-bottom:7px">
	<div class="col-xs-3"><input type="text" name='chat_username' value="<?php echo ($chat["user_username"]); ?>" readonly="true" class="col-xs-12"></div>
</div>
<div class="row" style="margin-bottom:7px">
	<div class="col-xs-3"><input type="text" name='chat_username2' value="<?php echo ($chat["user_username2"]); ?>" readonly class="col-xs-12"></div>
</div>
<div class="row" style="margin-bottom:7px">
	<div class="col-xs-12">
		<textarea id="form-field-11" class="autosize-transition form-control" style="overflow:hidden;word-wrap:break-word;resize:horizontal;height:60px;" readonly name='chat_content'><?php echo ($chat["chat_content"]); ?></textarea>
	</div>
</div>
<div class="row" style="margin-bottom:7px">
	<div class="col-xs-3"><input type="text" name='chat_time' value="<?php echo ($chat["chat_time"]); ?>" readonly class="col-xs-12"></div>
</div>

<span class="btn" onClick="location.href='/index.php/Home/Chat/updateChat/user_id/<?php echo ($user_id); ?>'">返回</span>
</form>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<div class="footer">
				<div class="footer-inner">
					<!-- #section:basics/footer -->
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">人人算PC端后台管理系统</span>
							2015.12.22 &copy; 2015 -
						</span>

						&nbsp; &nbsp;
						<span class="action-buttons">
							<a href="#">
								<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
							</a>
						</span>
					</div>

					<!-- /section:basics/footer -->
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='/Public/assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='/Public/assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='/Public/assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="/Public/assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->

		<!-- ace scripts -->
		<script src="/Public/assets/js/ace/elements.scroller.js"></script>
		<script src="/Public/assets/js/ace/elements.colorpicker.js"></script>
		<script src="/Public/assets/js/ace/elements.fileinput.js"></script>
		<script src="/Public/assets/js/ace/elements.typeahead.js"></script>
		<script src="/Public/assets/js/ace/elements.wysiwyg.js"></script>
		<script src="/Public/assets/js/ace/elements.spinner.js"></script>
		<script src="/Public/assets/js/ace/elements.treeview.js"></script>
		<script src="/Public/assets/js/ace/elements.wizard.js"></script>
		<script src="/Public/assets/js/ace/elements.aside.js"></script>
		<script src="/Public/assets/js/ace/ace.js"></script>
		<script src="/Public/assets/js/ace/ace.ajax-content.js"></script>
		<script src="/Public/assets/js/ace/ace.touch-drag.js"></script>
		<script src="/Public/assets/js/ace/ace.sidebar.js"></script>
		<script src="/Public/assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="/Public/assets/js/ace/ace.submenu-hover.js"></script>
		<script src="/Public/assets/js/ace/ace.widget-box.js"></script>
		<script src="/Public/assets/js/ace/ace.settings.js"></script>
		<script src="/Public/assets/js/ace/ace.settings-rtl.js"></script>
		<script src="/Public/assets/js/ace/ace.settings-skin.js"></script>
		<script src="/Public/assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="/Public/assets/js/ace/ace.searchbox-autocomplete.js"></script>

		<!-- inline scripts related to this page -->

		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="/Public/assets/css/ace.onpage-help.css" />
		<link rel="stylesheet" href="/Public/docs/assets/js/themes/sunburst.css" />

		<script type="text/javascript"> ace.vars['base'] = '/Public'; </script>
		<script src="/Public/assets/js/ace/elements.onpage-help.js"></script>
		<script src="/Public/assets/js/ace/ace.onpage-help.js"></script>
		<script src="/Public/docs/assets/js/rainbow.js"></script>
		<script src="/Public/docs/assets/js/language/generic.js"></script>
		<script src="/Public/docs/assets/js/language/html.js"></script>
		<script src="/Public/docs/assets/js/language/css.js"></script>
		<script src="/Public/docs/assets/js/language/javascript.js"></script>
		
<script src="/Public/assets/js/select2.js"></script>

		

<script type="text/javascript">

    $(function(){
        $('#receiveMenu6').addClass('active open');
        $('#appointmentItem61').addClass('active');
    });
</script>

	</body>
</html>