<?php
namespace Home\Controller;
use Think\Controller;
class SystemController extends Controller {
	public function index() {
        $this->display();
    }
    public function autocomplete() {
    	$this->display();
    }
    public function news() {
    	$this->display();
    }
    public function gallery() {
    	$this->display();
    }
    public function category() {
        $this->display();
    }
    public function category_add() {
        $this->display();
    }
    public function category_action() {
        $img = explode(".", $_FILES["lookname"]["name"]);
        $extension = $img[count($img)-1];
        $filepath = "./uploads/";
        $randname = date("Y").date("m").date("d").date("H").date("i").date("s").rand(100,999).".".$extension;
        if (move_uploaded_file($_FILES["lookname"]["tmp_name"], $filepath.$randname)) {
            $data['category_pic'] = $randname;
        }
        $img2 = explode(".", $_FILES["lookname2"]["name"]);
        $extension2 = $img2[count($img2)-1];
        $filepath2 = "./uploads/";
        $randname2 = date("Y").date("m").date("d").date("H").date("i").date("s").rand(100,999).".".$extension2;
        if (move_uploaded_file($_FILES["lookname2"]["tmp_name"], $filepath2.$randname2)) {
            $data['category_pic2'] = $randname2;
        }
        $data['category_name'] = I('name');
        $data['category_topdate'] = time();
        $data['category_date'] = time();
        if (M('category')->add($data)) {
            $this->redirect('category_list');
        }
    }
    public function category_list() {
        $category = M('category')->order('category_top desc,category_topdate desc')->select();
        $this->category = $category;
        $this->display();
    }
    public function category_del() {
        $category_id = I('category_id');
        if (M('category')->where("category_id=$category_id")->delete()) {
            $this->redirect('category_list');
        }
    }
    public function category_update() {
        $category_id = I('category_id');
        $category = M('category')->where("category_id=$category_id")->find();
        $this->category = $category;
        $this->display();
    }
    public function category_update_action() {
        $category_id = I('category_id');
        if (!empty($_FILES['lookname']['tmp_name'])) {
            $img = explode(".", $_FILES["lookname"]["name"]);
            $extension = $img[count($img)-1];
            $filepath = "./uploads/";
            $randname = date("Y").date("m").date("d").date("H").date("i").date("s").rand(100,999).".".$extension;
            if(move_uploaded_file($_FILES["lookname"]["tmp_name"], $filepath.$randname)){
                $data['category_pic'] = $randname;
            }
        }
        if (!empty($_FILES['lookname2']['tmp_name'])) {
            $img2 = explode(".", $_FILES["lookname2"]["name"]);
            $extension2 = $img2[count($img2)-1];
            $filepath2 = "./uploads/";
            $randname2 = date("Y").date("m").date("d").date("H").date("i").date("s").rand(100,999).".".$extension2;
            if(move_uploaded_file($_FILES["lookname2"]["tmp_name"], $filepath2.$randname2)){
                $data['category_pic2'] = $randname2;
            }
        }
        $data['category_name'] = I('name');
        if (M('category')->where("category_id=$category_id")->save($data) !== false) {
            $this->redirect('category_list');
        }
    }
    public function category_topds() {
        $category_id = I('category_id');
        $category_top = I('top');
        if ($category_top == '1') {
            $data = array('category_top'=>'2','category_topdate'=>time());
        } else {
            $data = array('category_top'=>'1','category_topdate'=>time());
        }
        M('category')->where("category_id=$category_id")->save($data);
        $this->redirect('category_list');
    }
    /***********************************************************************************************/
    public function type() {
        $this->display();
    }
    public function type_add() {
        $this->display();
    }
    public function type_action() {
        $img = explode(".", $_FILES["lookname"]["name"]);
        $extension = $img[count($img)-1];
        $filepath = "./uploads/";
        $randname = date("Y").date("m").date("d").date("H").date("i").date("s").rand(100,999).".".$extension;
        if(move_uploaded_file($_FILES["lookname"]["tmp_name"], $filepath.$randname)){
            $data['type_name'] = I('name');
            $data['type_pic'] = $randname;
            $data['type_topdate'] = time();
            $data['type_date'] = time();
            if (M('type')->add($data)) {
                $this->redirect('type_list');
            }
        }
    }
    public function type_list() {
        $type = M('type')->order('type_top desc,type_topdate desc')->select();
        $this->type = $type;
        $this->display();
    }
    public function type_del() {
        $type_id = I('type_id');
        if (M('type')->where("type_id=$type_id")->delete()) {
            $this->redirect('type_list');
        }
    }
    public function type_update() {
        $type_id = I('type_id');
        $type = M('type')->where("type_id=$type_id")->find();
        $this->type = $type;
        $this->display();
    }
    public function type_update_action() {
        $type_id = I('type_id');
        if (!empty($_FILES['lookname']['tmp_name'])) {
            $img = explode(".", $_FILES["lookname"]["name"]);
            $extension = $img[count($img)-1];
            $filepath = "./uploads/";
            $randname = date("Y").date("m").date("d").date("H").date("i").date("s").rand(100,999).".".$extension;
            if(move_uploaded_file($_FILES["lookname"]["tmp_name"], $filepath.$randname)){
                $data['type_pic'] = $randname;
            }
        }
        $data['type_name'] = I('name');
        if (M('type')->where("type_id=$type_id")->save($data) !== false) {
            $this->redirect('type_list');
        }
    }
    public function type_topds() {
        $type_id = I('type_id');
        $type_top = I('top');
        if ($type_top == '1') {
            $data = array('type_top'=>'2','type_topdate'=>time());
        } else {
            $data = array('type_top'=>'1','type_topdate'=>time());
        }
        M('type')->where("type_id=$type_id")->save($data);
        $this->redirect('type_list');
    }

	//关于人人算
	public function about(){
		$info=M("about")->where("about_id=1")->find();
		$this->assign("info",$info);
		$this->display();
	}
	public function about_pro(){
		$data["about_content"] =$_REQUEST["about_content"];
		$reg=M("about")->where("about_id=1")->save($data);
		$this->redirect("about");
	}
	//人人算协议
	public function agreement(){
		if(IS_POST){
			$data["about_content"] =$_REQUEST["about_content"];
			$reg=M("about")->where("about_id=2")->save($data);
			$this->redirect();
		}else{
			$info=M("about")->where("about_id=2")->find();
			$this->assign("info",$info);
			$this->display();
		}
		
	}
}