<?php
namespace Home\Controller;
use Think\Controller;
/**
* 
*/
class WenBoController extends Controller
{
	
	public function findAllPic(){

		$layer = M('layer')
				->field('layer_id,layer_link,layer_pic')
				->select();
		$this->p = $p;
    	$this->assign('layer', $layer);
		$this->display();

	} 

	public function uploadImg() {

		$layer_id = $_GET['layer_id'];
		$layer = M('layer')->where("layer_id=$layer_id")->find();
    	$this->assign('layer_id', $layer_id);
    	$this->assign('layer', $layer);
		$this->display();    	
		
	}

	private function getLayerByLayerId($layer_id){

		$layer= M('layer')->where("layer_id=$layer_id")->find(); // 根据条件更新记录
		return $layer;

	}

	public function uploadImgAction(){
			//获取网页地址 
			$url = $_SERVER['PHP_SELF'];
			$str = explode('/',$url);
			$layer_id = end($str);

		    $upload = new \Think\Upload();// 实例化上传类
		    $upload->maxSize   =     3145728 ;// 设置附件上传大小
		    $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
		    $upload->rootPath  =     './Uploads/'; // 设置附件上传根目录
		    $upload->savePath  =     ''; // 设置附件上传（子）目录
		    $upload->saveName = time().'_APP';
		    $upload->autoSub = true;
			$upload->subName = date("Ym",time());

		    // 上传文件 
		    $info   =   $upload->upload();
		    if(!$info) {// 上传错误提示错误信息
		        $this->error($upload->getError());
		    }else{// 上传成功
		    	$layer_pic = '/uploads/'.$info['file']['savepath'].$info['file']['savename'];
		    	$this->updateLayer($layer_id,$layer_pic);
		        $this->success('上传成功！');
		        $this->redirect('findAllPic');
		    }
	}

	private function updateLayer($layer_id,$layer_pic){

		$layer = M('layer');
		$data['layer_pic'] = $layer_pic;

		if (isset($layer_link)) {
			$data['layer_link'] = $layer_link;
		}else{
			unset($layer_link);
		}
		if (isset($layer_top)) {
			$data['layer_top'] = $layer_top;
		}else{
			unset($layer_top);
		}
		if (isset($layer_topdate)) {
			$data['layer_topdate'] = $layer_topdate;
		}else{
			unset($layer_topdate);
		}
		$layer->where("layer_id=$layer_id")->save($data); // 根据条件更新记录
	}

	public function addImage($layer_link,$layer_pic){
		$layer = M('layer');
		$data['layer_link'] = $layer_link;
		$data['layer_pic'] = $layer_pic;
		$data['layer_top'] = $layer_top;
		$data['layer_topdate'] = time();
		$data['layer_date'] = time();
		$layer->add($data);

	}

	public function detailLayer(){
		$layer_id = I('layer_id');
		$layer = M('layer')->where("layer_id=$layer_id")->find();
		$this->assign('layer',$layer);

		$this->display();
	}
}