<?php
namespace Home\Controller;
use Think\Controller;
class ContentController extends Controller {
	//某个大师的评论列表
	public function content_list(){
		$user=M("user");
		$p = I('p');
		$user_id=I("user_id");
    	$count = M('ask')->where('ask_user_id='.$user_id)->count();
    	$page = getpage($count);
    	$content = M('ask')
			      ->where('ask_user_id='.$user_id)
			      ->order('ask_date desc')
			      ->limit($page->firstRow, $page->listRows)
			      ->select();
		foreach($content as $key=>$val){
			$content[$key]["user_username"]=$user->where("user_id=".$val["ask_user_id2"])->getField("user_username");
			$content[$key]["user_pic"]=$user->where("user_id=".$val["ask_user_id2"])->getField("user_pic");
		}
    	$this->p = $p;
    	$this->user_id = $user_id;
    	$this->assign('content', $content);
        $this->assign('page', $page->show());
    	$this->display();
	}
	//删除评论
	public function content_del() {
    	$ask_id = I('ask_id');
		//echo $ask_id;die;
		$user_id=I("user_id");
    	M('ask')->where("ask_id=$ask_id")->delete();
		echo "<script>location.href='/index.php/Home/Content/content_list/user_id/".$user_id."'</script>";
    }
	//
}