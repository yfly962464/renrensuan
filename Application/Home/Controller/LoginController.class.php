<?php
namespace Home\Controller;
use Think\Controller;
class LoginController extends Controller {
	//登录
	public function index(){

		if(IS_POST){
			$data=array();
			$data['username']=trim($_POST['username']);
			$data['password']=trim($_POST['password']);
			$sql=M('admin')->where(array('username'=>$_POST['username'],'password'=>$_POST['password']))->find();
			if($sql){
				session('id',$sql['id'],86400);
				$this->success('登陆成功',"/index.php/Home/System/category_list");
			}else{
				$this->error('登录失败');
			}
		}else{
			$this->display();
		}
		
	}
	//修改密码
	public function save_password(){
		//echo $_SESSION["id"];die;
		if(IS_POST){
			$old_pwd=$_POST["old_pwd"];
			$new_pwd=$_POST["new_pwd"];
			$confirm_pwd=$_POST["confirm_pwd"];
			$pwd=M("admin")->where("id=".$_SESSION["id"])->getField("password");
			//echo M("admin")->getLastSql();die;
			if($old_pwd!=$pwd){
				$this->error("您的密码输入不正确");
			}else if($new_pwd!=$confirm_pwd){
				$this->error("您两次输入不匹配");
			}else{
				$data["password"]=$new_pwd;
				if(M("admin")->where("id=".$_SESSION["id"])->save($data)){
					$this->success("修改成功");
				}else{
					$rhis->error("修改失败");
				}
			}
		}else{
			$this->display();
		}	
	}

   //添加用户
	public function login_add(){
		if(IS_POST){
			$data['username']=$_POST['username'];
			$data['password']=$_POST['password'];
			$sql=M('admin')->add($data);
			if($sql){
				$this->success('添加成功');
			}else{
				$this->error('添加失败');
			}
		}else{
			$this->display();
		}
		
	}
		public function liste(){
		$sql=M('admin')->select();
		//var_dump($sql);exit;
		$this->assign('sql',$sql);
		$this->display();
	}
  //删除用户
	public function del(){
		$user_id=I('id');
		//$user_id=$_GET['id'];
		//var_dump($user_id);exit;
		$sql=M('admin')->where(array('id'=>$user_id))->delete();
		$this->redirect('liste');
	}
	//登出
	public function loginout(){
        session('id',null);
        $this->success('退出成功','__GROUP__/Login/index');
    }


}