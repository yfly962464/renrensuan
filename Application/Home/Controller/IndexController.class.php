<?php
namespace Home\Controller;
use Think\Controller;
import("Org.Util.Easemob");
class IndexController extends Controller {
    var $options = array(

        'client_id' => 'YXA6NOdKUNteEeWN2q0JyRreYg',
        'client_secret' => 'YXA6ulVy04afB-feVWZW4CzMdDekKjo',
        'org_name' => '2011-fangzhou',
        'app_name' => 'forecast',

    );

    public function index() {
        $this->display();
    }
    public function users() {

        $p = I('p');
        //echo $p;die;
        $count = M('user')->where('user_identity_id=1')->count();
        $page = getpage($count);
        $users = M('user')->where('user_identity_id=1')->order('user_top desc,user_topdate desc')->limit($page->firstRow, $page->listRows)->select();
        $this->p = $p;
        $this->assign('users', $users);
        $this->assign('page', $page->show());
        $this->display();
    }
    public function ordinary_user(){
        $p = I('p');
        $count = M('user')->where('user_identity_id=0')->count();
        $page = getpage($count);
        $users = M('user')->where('user_identity_id=0')->order('user_top desc,user_topdate desc')->limit($page->firstRow, $page->listRows)->select();
        $this->p = $p;
        $this->assign('users', $users);
        $this->assign('page', $page->show());
        $this->display();
    }
    public function users_add() {
        $this->display();
    }
    public function users_action() {
        

        $user_tel = I('tel');
        $user_link = I('pwd');
        $user_pwd = md5($user_link);

        $data['user_tel'] = $user_tel;
        $data['user_link'] = $user_link;
        $data['user_pwd'] = $user_pwd;
        $data['user_identity_id'] = 1;
        $data['user_date'] = time();
        $h = new \Org\Util\Easemob($this->options);
        
        if (M('user')->add($data)) {
            $h->createUser($user_tel,$user_link);            
            $result = $this->redirect('users');
        } else {
            $this->error('系统错误！');
        }
    }
    public function users_top() {
        $user_top = I('top');
        $user_id = I('user_id');
        if ($user_top == '1') {
            $data['user_top'] = '2';
            $data['user_topdate'] = time();
        } else {
            $data['user_top'] = '1';
            $data['user_topdate'] = time();
        }
        M('user')->where("user_id=$user_id")->save($data);
        $this->redirect('users');
    }
    public function users_del() {
        $user_id = I('user_id');
        M('user')->where("user_id=$user_id")->delete();
        $this->redirect('users');
    }
    public function users_edit() {
        $user_id = I('user_id');
        $p = I('p');
        $users = M('user')->where("user_id=$user_id")->find();
        if ($users) {
            $this->p = $p;
            $this->user_id = $user_id;
            $this->users = $users;
            $this->display();
        } else {
            $this->error('系统错误！');
        }
    }
    public function users_edit_as() {
        $user_id = I('user_id');
        $category = M('category')->field('category_id,category_name')->select();
        $user = M('user')->field('user_speciality')->where("user_id=$user_id")->find();
        $speciality = array_filter(explode(',', $user['user_speciality']));
        echo json_encode(array('code'=>1,'speciality'=>$speciality,'category'=>$category));
    }
    public function users_edit_action() {
        $p = I('p');
        $user_id = I('user_id');
        if ($p == '') $this->error('唉呀，出错啦！');
        if ($user_id == '') $this->error('唉呀，出错啦！');
        //print_R($_FILES);DIE;
        if (!empty($_FILES['lookname']['tmp_name'])) {
            $img = explode(".", $_FILES["lookname"]["name"]);
            $extension = $img[count($img)-1];
            $filepath = './uploads/'.date('Ym').'/';
            $randname = date("Y").date("m").date("d").date("H").date("i").date("s").rand(100,999).".".$extension;
            //echo $filepath.$randname;die;
            if(move_uploaded_file($_FILES["lookname"]["tmp_name"], $filepath.$randname)){
                
                $data['user_pic'] = trim($filepath.$randname,".");
            }
        }
        //print_R($data);die;
        $user_votes = I('user_votes');
        $user_value = I('user_value');
        $user_pct = trim(I('user_pct'),"%");
        //echo $user_pct;die;
        $user_name = I('user_name');
        $user_ccb = I('user_ccb');
        $user_card = I('user_card');
        $user_money = I('user_money');
        $user_tel = I('user_tel');
        $user_link = I('user_link');
        $user_username = I('user_username');
        $user_sex = I('user_sex');
        $user_broken = I('user_broken');
        $user_content = I('user_content');
        $user_speciality = I('speciality');
        $u = M('user');
        if ($usr = $u->field('user_link')->where("user_id=$user_id")->find()) {
            if ($usr['user_link'] != $user_link) {
                $data['user_link'] = $user_link;
                $data['user_pwd'] = md5($user_link);
            }
        } else {
            $data['user_link'] = $user_link;
            $data['user_pwd'] = md5($user_link);
        }
        $data['user_votes'] = $user_votes;
        $data['user_value'] = $user_value;
        $data['user_pct'] = $user_pct;
        $data['user_name'] = $user_name;
        $data['user_ccb'] = $user_ccb;
        $data['user_card'] = $user_card;
        $data['user_money'] = $user_money;
        $data['user_tel'] = $user_tel;
        $data['user_username'] = $user_username;
        $data['user_sex'] = $user_sex;
        $data['user_broken'] = $user_broken;
        $data['user_content'] = $user_content;
        $data['user_speciality'] = $user_speciality;
        //print_R($data);die;
        $u->where("user_id=$user_id")->save($data);
        $this->redirect("Home/Index/users/p/$p");
    }
    public function users_content_pic() {
        $p = I('p');
        $user_id = I('user_id');
        if ($p == '') $this->error('唉呀，出错啦3！');
        if ($user_id == '') $this->error('唉呀，出错了4！');
        $user_content_pic = M('user')->field('user_content_pic')->where("user_id=$user_id")->find();
        if (!empty($user_content_pic)) {
            $pic = array_filter(explode(',', $user_content_pic['user_content_pic']));
        } else {
            $pic = array();
        }
        $this->pic = $pic;
        $this->p = $p;
        $this->user_id = $user_id;
        $this->display();
    }
    public function users_content_pic_action() {
        $p = I('p');
        $user_id = I('user_id');
        //echo $p;exit;
        if ($p == '') {
            $this->error('唉呀，出错啦1！');
        }
        if ($user_id == '') {
            $this->error('唉呀，出错了2！');
        }
        if (!empty($_FILES['lookname']['tmp_name'])) {
            $img = explode(".", $_FILES["lookname"]["name"]);
            $extension = $img[count($img)-1];
            $filepath = "./uploads/";
            $randname = date("Y").date("m").date("d").date("H").date("i").date("s").rand(100,999).".".$extension;
            if (move_uploaded_file($_FILES["lookname"]["tmp_name"], $filepath.$randname)) {
                $user_content_pic = M('user')->field('user_content_pic')->where("user_id=$user_id")->find();
                if ($user_content_pic) {
                    $data['user_content_pic'] = $user_content_pic['user_content_pic'].$randname.',';
                } else {
                    $data['user_content_pic'] = $randname.',';
                }
                M('user')->where("user_id=$user_id")->save($data);
            }
        }
        $this->redirect("Home/Index/users_content_pic/user_id/$user_id/p/$p");
    }
    public function img_del() {
        $img = I('img');
        $user_id = I('user_id');
        $p = I('p');
        if ($img != '' && $user_id != '' && $p != '') {
            $u = M('user');
            $user_content_pic = $u->field('user_content_pic')->where("user_id=$user_id")->find();
            $pic = $user_content_pic['user_content_pic'];
            $pic = str_replace($img.',', '', $pic);
            $data['user_content_pic'] = $pic;
            $u->where("user_id=$user_id")->save($data);
        }
        $this->redirect("Home/Index/users_content_pic/user_id/$user_id/p/$p");
    }
    public function users_look() {
        $p = I('p');
        $user_id = I('user_id');
        $users = M('user')->where("user_id=$user_id")->find();
        //print_R($users);die;
        $user_speciality = rtrim($users['user_speciality'], ',');
        $map['category_id'] = array ('in', $user_speciality);
        $category = M('category')->field('category_name')->where($map)->select();
        $this->category = $category;
        $this->p = $p;
        $this->user_id = $user_id;
        $this->users = $users;
        $this->display();
    }

    public function export(){

        $url = $_SERVER['PHP_SELF'];
        
        $str = explode('/',$url);
        $type = end($str);

        switch ($type) {
            case 'master':
                $users = M('user')
                    ->where('user_identity_id=1')
                    ->order('user_top desc,user_topdate desc')
                    ->select();
                break;
            case 'consumer':
                $users = M('user')
                    ->where('user_identity_id=0')
                    ->order('user_top desc,user_topdate desc')
                    ->select();                
                break;          
            default:
                $users = M('user')
                    ->where('user_identity_id=1')
                    ->order('user_top desc,user_topdate desc')
                    ->select();
                break;
        }

        $p = I('p');               
        $this->p = $p;
        $this->user_export($users,$type);
        $this->assign('users', $users);        
        $this->display();
    }

    //导出数据方法
    protected function user_export($user_list=array(),$type)
    {
        $user_list = $user_list;
        $data = array();
        foreach ($user_list as $k=>$user_info){

            $data[$k]['user_id'] = $user_info['user_id'];
            $data[$k]['user_identity_id'] = $user_info['user_identity_id'];
            $data[$k]['user_money'] = $user_info['user_money'];            
            $data[$k]['user_tel']  = $user_info['user_tel'];
            $data[$k]['user_username']  = $user_info['user_username'];

            $data[$k]['user_sex']  = $user_info['user_sex'];
            $data[$k]['user_date']  = $user_info['user_date']; 

            switch ($type) {
                case 'master':
                    $data[$k]['total_money']  = $user_info['total_money'];
                    $data[$k]['user_votes']  = $user_info['user_votes'];
                    $data[$k]['user_value']  = $user_info['user_value'];
                    $data[$k]['user_pct']  = $user_info['user_pct'];
                    $data[$k]['user_speciality']  = $user_info['user_speciality'];
                    $data[$k]['user_broken']  = $user_info['user_broken'];
                    $data[$k]['user_content']  = $user_info['user_content'];
                    break;
                case 'consumer':
                    $data[$k]['total_consume'] = $user_info['total_consume']; 
                    break;
            }                       
            
        }

        foreach ($data as $field=>$v){

            if($field == 'user_id'){
                $headArr[]='用户编号';
            }
            if($field == 'user_identity_id'){
                $headArr[]='用户类别';
            }
            if($field == 'user_money'){
                $headArr[]='用户余额';
            }
            
            if($field == 'user_tel'){
                $headArr[]='用户电话';
            }

            if($field == 'user_username'){
                $headArr[]='昵称';
            }
            if($field == 'user_sex'){
                $headArr[]='性别';
            }
            if($field == 'user_date'){
                $headArr[]='注册日期';
            }

            switch ($type) {
                case 'master':
                    if($field == 'total_money'){
                        $headArr[]='总收入';
                    }
                    if($field == 'user_votes'){
                        $headArr[]='总投票';
                    }
                    if($field == 'user_value'){
                        $headArr[]='评论总分数';
                    }
                    if($field == 'user_pct'){
                        $headArr[]='评论百分比';
                    }
                    if($field == 'user_speciality'){
                        $headArr[]='擅长集群';
                    }
                    if($field == 'user_broken'){
                        $headArr[]='个性签名';
                    }
                    if($field == 'user_content'){
                        $headArr[]='个人简介';
                    }
                    break;
                case 'consumer':
                    if($field == 'total_consume'){
                        $headArr[]='总消费';
                    }
                    break;                
                default:
                    if($field == 'total_money'){
                        $headArr[]='总收入2222';
                    }
                    break;
            }            
        }        

        $filename="users";
        $this->getExcel($filename,$headArr,$data);
    }

    private  function getExcel($fileName,$headArr,$data){
        //导入PHPExcel类库，因为PHPExcel没有用命名空间，只能inport导入
        vendor('Excel.PHPExcel');
        vendor('Excel.PHPExcel.Writer.Excel5');
        vendor('Excel.PHPExcel.IOFactory.php');

        $date = date("Y_m_d",time());
        $fileName .= "_{$date}.xls";

        //创建PHPExcel对象，注意，不能少了\
        $objPHPExcel = new \PHPExcel();
        $objProps = $objPHPExcel->getProperties();

        //设置表头
        $key = ord("A");
        //print_r($headArr);exit;
        foreach($headArr as $v){
            $colum = chr($key);
            $objPHPExcel->setActiveSheetIndex(0) ->setCellValue($colum.'1', $v);
            $objPHPExcel->setActiveSheetIndex(0) ->setCellValue($colum.'1', $v);
            $key += 1;
        }

        $column = 2;
        $objActSheet = $objPHPExcel->getActiveSheet();

        //print_r($data);exit;
        foreach($data as $key => $rows){ //行写入
            $span = ord("A");
            foreach($rows as $keyName=>$value){// 列写入
                $j = chr($span);
                $objActSheet->setCellValue($j.$column, $value);
                $span++;
            }
            $column++;
        }

        $fileName = iconv("utf-8", "gb2312", $fileName);
        //重命名表
        //$objPHPExcel->getActiveSheet()->setTitle('test');
        //设置活动单指数到第一个表,所以Excel打开这是第一个表
        $objPHPExcel->setActiveSheetIndex(0);
        ob_end_clean();//清除缓冲区,避免乱码
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=\"$fileName\"");
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output'); //文件通过浏览器下载
        exit;
    }
}