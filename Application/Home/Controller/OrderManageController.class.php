<?php
namespace Home\Controller;
use Think\Controller;

	class OrderManageController extends Controller{

		public function showAllOrder(){

			$p = I('p');
			$orderCount = M('order')->count();

			$page = getpage($orderCount);
			$orders = $this->getAllOrderRecords();
			$orders = $this->getOrderName($orders);

			$this->p = $p;
	    	$this->assign('p', $p);
	    	$this->assign('orders', $orders);
	        $this->assign('page', $page->show());
	    	$this->display();	

		}

		private function getOrderName($orders){

			foreach ($orders as $key => $value) {

				$orders[$key]['charge_username']	= M('order')
							->field('user_username')
							->join('d_user on order_user_id = user_id')
							->where('user_id='.$orders[$key]['order_user_id'])
							->find();

				$orders[$key]['pay_username']	= M('order')
							->field('user_username')
							->join('d_user on order_user_id2 = user_id')
							->where('user_id='.$orders[$key]['order_user_id2'])
							->find();	
			}
			return $orders;
			
		}

		private function getAllOrderRecords(){

			$orders = M('order')
				->field('id,order_user_id,order_user_id2,order_balance,order_date,order_id')
				->join('d_user on order_user_id = user_id')
				->where('user_identity_id=1')
				->limit($page->firstRow, $page->listRows)
				->select();
				
			return $orders;
		}

		public function detailOrder(){

			$order_id = I('order_id');
			$p = I('p');
			$order = $this->getOrderDetail($order_id,$p);
			$order['p'] = $p;
			
			$this->assign('order', $order);

	    	$this->display();

		}

		public function editOrder(){

			$order_id = I('order_id');
			$p = I('p');
			$order = $this->getOrderDetail($order_id,$p);
			$this->assign('order', $order);
	    	$this->display();

		}

		public function deleteOrder(){

			$order_id = I('order_id');
			$order = $this->getOrderDetail($order_id,$p);

			if($order_id == ''){ 
	            echo json_encode(array('code'=>2));
	            exit;
        	}

			M('order')->where("id=$order_id")->delete();
			$this->redirect('showAllOrder');

		}

		private function getOrderDetail($order_id,$p){

			$order = M('order')
				->field('id,user_tel,order_user_id,order_user_id2,order_balance,order_date')
				->join('d_user on order_user_id = user_id')
				->where("id=$order_id")
				->find();
			$order_user_id = $order['order_user_id'];
			$order_user_id2 = $order['order_user_id2'];

			$chargeName = $this->getUserInfo($order_user_id);
			$order['chargeName'] = $chargeName['user_username'];
			$order['user_card'] = $chargeName['user_card'];
			$order['user_ccb'] = $chargeName['user_ccb'];
			$order['p'] = $p;
			
			$payname = $this->getUserInfo($order_user_id2);
			$order['payname'] = $payname['user_username'];
			$order['user_card2'] = $payname['user_card'];
			$order['user_ccb2'] = $payname['user_ccb'];
			$order['user_tel'] = $payname['user_tel'];
			return $order;

		}

		private function getUserInfo($user_id){

			$user = M('User')->field('user_username,user_card,user_ccb,user_tel')->where('user_id='.$user_id)->find();

			return $user;
		}

	}

?>