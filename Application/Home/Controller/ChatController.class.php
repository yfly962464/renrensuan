<?php
	namespace Home\Controller;
	use Think\Controller;

	class ChatController extends Controller{
		// 获取大师名单列表
		public function masterList(){

			if ($this->hasMasterRecord()) {

				$user = $this->getMasterList();
				$page = getpage(count($user));
				$this->p = $p;
				$this->assign('user',$user);
		        $this->assign('page', $page->show());
				$this->display();

			}			

		}
		// 获取用户名单列表
		public function customerList(){
			$master_id = $_GET['user_id'];

			if ($this->hasCustomerRecord($master_id)) {
				
				$customer = $this->getCustomerList($master_id);
				$page = getpage(count($customer));
				$this->p = $p;
				$this->assign('customer',$customer);
		        $this->assign('page', $page->show());
				$this->display();
			}

		}

		// 判断与某个大师有过聊天的用户名单是否存在
		private function hasCustomerRecord($master_id){

			return (!empty($this->getCustomerList($master_id))) ? ture :false;

		}
		// 得到与某个大师有过聊天的用户名单
		private function getCustomerList($master_id){
			$masterName = M('user')
						->field('user_username')
						->where("user_id=$master_id")
						->find();

			$user = M('user')
					->field('user_username,user_id')
					->join('d_chatCheck on check_user_id2 = user_id')
					->where("check_user_id=$master_id")
					->distinct(true)
					->select();
			$user['masterName'] = $masterName['user_username'];		
			return $user;		
		}

		// 得到有过聊天记录大师的名单
		private function getMasterList(){
			$user = M('user')
					->field('user_username,check_user_id')
					->join('d_chatCheck on user_id = check_user_id')
					->distinct(true)
					->select();
			return $user;
		}
		// 判断是否有聊天过的大师
		private function hasMasterRecord(){
			return (!empty($this->getMasterList())) ? ture :false;
		}

		public function addChat(){
			
			$chat_user_id = I('chat_user_id');
			$chat_user_id2 = I('chat_user_id2');
			$chat_content = I('chat_content');
			$this->doAddChat($chat_user_id,$chat_user_id2,$chat_content);

			$this->redirect('masterList');
			$this->display();

		}

		private function doAddChat($chat_user_id,$chat_user_id2,$chat_content){
			$chat = M('chat');

			if ($this->hasChatCheck($chat_user_id,$chat_user_id2)) {
				
				$chatCheck = M('chatCheck');
				$data['check_user_id'] = $chat_user_id;
				$data['check_user_id2'] = $chat_user_id2;
				$chatCheck->add($data);
			}

			$data['chat_user_id'] = $chat_user_id;
			$data['chat_user_id2'] = $chat_user_id2;
			$data['chat_time'] = time();
			$chat->add($data);
		}

		private function getChatCheck($chat_user_id,$chat_user_id2){

			$chatCheck = M('chatCheck')
					->where("check_user_id=$check_user_id and check_user_id2=check_user_id2")
					->find();
			return $chatCheck;		

		}

		private function hasChatCheck($chat_user_id,$chat_user_id2){

			return (empty($this->getChatCheck($chat_user_id,$chat_user_id2)))? false:true;

		}
		// 显示某个大师与特定用户的聊天记录
		function showAllChat(){

			$customer_id = $_GET['user_id'];

			$count = M('chat')->count();
    		$page = getpage($count);
			$chat = M('chat')
				->field('id,chat_user_id,chat_user_id2,chat_time,chat_content')
				->where("chat_user_id2=$customer_id")
				->order('chat_time')
				->limit($page->firstRow, $page->listRows)			
				->select();
			$chat = $this->getAllChats($chat);

			$this->p = $p;
			$this->assign('chat', $chat);
        	$this->assign('page', $page->show());
			$this->display();

		}

		private function getAllChats($chat){
			foreach ($chat as $key => $value) {	
				// send user's name
				$chat_username = M('user')
						->field('user_username')
						->where('user_id='.$value['chat_user_id'])
						->find();	
				$chat[$key]['chat_username'] = $chat_username['user_username'];
				// response user's name 
				$chat_username2 = 	$chat_username = M('user')
						->field('user_username')
						->where('user_id='.$value['chat_user_id2'])
						->find();
						
				$chat[$key]['chat_username2'] = $chat_username2['user_username'];		
			}
			return $chat;
		}

		public function detailChat(){
						
			$chat_id = I('chat_id');			
			$chat = $this->getChatByCharId($chat_id);
			$this->assign('chat', $chat);

			$this->display();

		}

		private function getChatByCharId($chat_id){

			$chat = M('chat')
					->field('id,user_username,chat_content,chat_time,chat_user_id,chat_user_id2')
					->join('d_user on chat_user_id = user_id')
					->where("id=$chat_id")
					->find();						
			$username = M('chat')
					->field('user_username')
					->join('d_user on chat_user_id2 = user_id')
					->where("id=$chat_id")
					->find();
			$chat['user_username2'] = $username['user_username'];

			return $chat;
		}

		public function showUpdatePage(){

			$chat_id = I('chat_id');			
			$chat = $this->getChatByCharId($chat_id);
			$this->assign('chat', $chat);

			$this->display('updateChatContent');


		}

		public function updateChatContent(){

	    	$chat_id = I('chat_id');
	    	$chat_content = $_POST['chat_content'];

	    	$chat = M('chat')->where("id=$chat_id");

	    	if ($chat) {
	    		$chat->setField('chat_content',$chat_content);
	    		$this->redirect("showAllChat");
	    	} else {
	    		$this->error('系统错误！');
	    	}
	    	

		}

		public function deleteChatByChatId(){

			$chat_id = I('chat_id');
			M('chat')->where("id=$chat_id")->delete();
     		$this->redirect('showAllChat');

		}

		public function deleteChatByUserId(){

			$chat_id = I('chat_id');
			M('chat')->where("chat_user_id=$chat_id or chat_user_id2=$chat_id")->delete();

		}

	}

?>