<?php
	namespace Home\Controller;
	use Think\Controller;
	class PrivateMessageController extends Controller{

		// search all private messages
		public function message_list(){

			$p = I('p');
			$message_user_id = I('message_user_id');
			$message_user_id2 = I('message_user_id2');

			$user=M("user");
			$count = M('message')->count();
    		$page = getpage($count);

			$message = $this->getAllMessageLists($message_user_id,$message_user_id2,$user,$count,$page);

			$this->p = $p;
	    	$this->assign('message', $message);
	        $this->assign('page', $page->show());
	    	$this->display();		

		}

		// get all messages by userid
		private function getAllMessageLists($message_user_id,$message_user_id2,$user,$count,$page){ 

			$message = M('message')
				->field('message_id,message_parent_id,message_content,user_username,message_status,user_pic,message_date')
				->join('d_user ON user_id = message_user_id')	
				->where("message_user_id=$message_user_id and message_user_id2=$message_user_id2")
				->order('message_date asc')
				->select();

			$message2 = M('message')
				->field('message_id,message_parent_id,message_content,user_username,message_status,user_pic,message_date')
				->join('d_user ON user_id = message_user_id2')	
				->where("message_user_id=$message_user_id and message_user_id2=$message_user_id2 and user_id=$message_user_id2")
				->select();	

			foreach($message as $key=>$val){

				if (1==$message[$key]['message_status']) {
					$message[$key]["user_username"] = $message[$key]["user_username"];
					$message[$key]["pic"] = $message[$key]["user_pic"];
				}else{
					$message[$key]["user_username"] = '我';
					$message[$key]['pic'] = $message2[$key]["user_pic"];
				}
			};

			return $message;
		}

	}

?>