<?php
namespace Home\Controller;
use Think\Controller;
class ArticleController extends Controller {
	//文章列表
	public function article_list(){
		$p = I('p');
		//echo $p;die;
		$user=M("user");
    	$count = M('article')->count();
    	$page = getpage($count);
    	$article = M('article')->order('article_date desc')->limit($page->firstRow, $page->listRows)->select();
		foreach($article as $key=>$val){
			$article[$key]["user_username"]=$user->where("user_id=".$val["article_user_id"])->getField("user_username");
			$article[$key]["user_pic"]=$user->where("user_id=".$val["article_user_id"])->getField("user_pic");
			$article[$key]["type_name"]=M("type")->where("type_id=".$val["article_type_id"])->getField("type_name");
		}
		//print_R($article);die;
    	$this->p = $p;
    	$this->assign('article', $article);
        $this->assign('page', $page->show());
    	$this->display();
	}

	public function article_detail(){
		$p = I('p');
		$user=M("user");
        $article_id = I('article_id');
        $article = M('article')->where("article_id=$article_id")->find();
		$article["user_username"]=$user->where("user_id=".$article["article_user_id"])->getField("user_username");
		$article["user_pic"]=$user->where("user_id=".$article["article_user_id"])->getField("user_pic");
		$article["type_name"]=M("type")->where("type_id=".$article["article_type_id"])->getField("type_name");
		$article["praise_count"]=M("praise")->where("praise_article_id=".$article_id)->count();
		$article["collection_count"]=M("scwz")->where("scwz_article_id=".$article_id)->count();
		$article["comment_count"]=M("talk")->where("talk_article_id=".$article_id)->count();
        $this->p = $p;
        $this->user_id = $article["article_user_id"];
        $this->article = $article;
        $this->display();
	}
  //审核通过
  public function up_status(){
     $article_id=I("article_id");
	 $p=I("p");
	 $data["article_status"]=1;
	 M("article")->where("article_id=".$article_id)->save($data);
	 $this->redirect('article_list');
  }
  //删除文章
  public function article_del(){
	 $article_id=I("article_id");
	 M('article')->where("article_id=$article_id")->delete();
     $this->redirect('article_list');
  }

  //文章置顶/不置顶
  public function article_top(){
	$article_id = I('article_id');
	$article_top = I('top');
	if ($article_top == '1') {
		$data = array('article_top'=>'2','article_topdate'=>time());
	} else {
		$data = array('article_top'=>'1','article_topdate'=>time());
	}
	M('article')->where("article_id=$article_id")->save($data);
	$this->redirect('article_list');
  }
  	public function addArticle(){

		$article_type = M('type')->select();
 		$this->assign('article_type',$article_type);
		$this->display();

	}

 	public function addArticleAction(){

 		$article = M('article');
  		$data['article_type_id'] = $_POST['article_type_id'];
  		$data['article_title'] = $_POST['article_title'];
  		$data['article_content'] = $_POST['article_content'];
  		$info = $this->uploadImg();

  		if(!$info) {// 上传错误提示错误信息
	        $this->error($upload->getError());
	    }else{// 上传成功
	    	$article_pic = '/uploads/'.$info['article_pic']['savepath'].$info['article_pic']['savename'];    	
	    	$data['article_pic'] = $article_pic;	        
	    }
  		$data['article_date'] = time();
  		$data['article_topdate'] = time();
  		$article->add($data);
  		
  		$this->redirect('article_list');
 	}

 	public function updateArticle(){
 		$article_id = I('article_id');

 		$article_type = $this->getArticleType();
 		$this->assign('article_type',$article_type);

 		$article = M('article')->where("article_id=$article_id")->find();
 		$this->assign('article',$article);

 		$this->display();

 	}
 	private function getArticleType(){
 		$article_type = M('type')->select();
 		return $article_type;
 	}

 	function updateArticleAction(){

 		$article = M('article');
 		$article_id = $_GET["article_id"];
  		$data['article_type_id'] = $_POST['article_type_id'];

  		$data['article_title'] = $_POST['article_title'];
  		$data['article_content'] = $_POST['article_content'];
  		$info = $this->uploadImg();

  		if(!$info) {// 上传错误提示错误信息
	        $data['article_pic'] = $article
			    		->where("article_id=$article_id")
			    		->getField('article_pic');
	    }else{// 上传成功
	    	$article_pic = '/uploads/'.$info['article_pic']['savepath'].$info['article_pic']['savename'];
	    	$data['article_pic'] = $article_pic;  	
	    	        
	    }

  		$article->where("article_id=$article_id")->save($data);  		
  		$this->redirect('article_list');
 	}

 	private function uploadImg(){
			//获取网页地址 
		    $upload = new \Think\Upload();// 实例化上传类
		    $upload->maxSize   =     3145728 ;// 设置附件上传大小
		    $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
		    $upload->rootPath  =     './Uploads/'; // 设置附件上传根目录
		    $upload->savePath  =     ''; // 设置附件上传（子）目录
		    $upload->saveName = time().'_APP';
		    $upload->autoSub = true;
			$upload->subName = date("Ym",time());

		    // 上传文件 
		    $info   =   $upload->upload();
		    return $info;
		    
	}	
}