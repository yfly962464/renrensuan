-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2016-01-25 06:56:23
-- 服务器版本： 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `renrensuan`
--

-- --------------------------------------------------------

--
-- 表的结构 `d_article`
--

CREATE TABLE IF NOT EXISTS `d_article` (
  `article_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `article_user_id` int(11) NOT NULL COMMENT '关联user表大师的user_id',
  `article_type_id` int(11) NOT NULL COMMENT '关联type表大师的type_id',
  `article_pic` varchar(256) DEFAULT '' COMMENT '图像路径 仅一张图像',
  `article_title` varchar(32) DEFAULT '' COMMENT '文章标题',
  `article_content` varchar(500) DEFAULT '' COMMENT '文章内容',
  `article_date` int(11) NOT NULL COMMENT '文章发表日期 时间戳',
  `article_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未审核 1已审核',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='大师文章表' AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `d_article`
--

INSERT INTO `d_article` (`article_id`, `article_user_id`, `article_type_id`, `article_pic`, `article_title`, `article_content`, `article_date`, `article_status`) VALUES
(1, 1, 4, '/uploads/201601/1453102377_APP.jpg', '论八字七杀的力量', '八字见七杀其实并不可怕，七杀往往代表着淫荡的权威', 1453095957, 1),
(2, 1, 4, '/uploads/201601/1453095957_APP.jpg', '浅谈四柱八字中的正宫', '正宫意味着权利，有正宫着可身居高位', 1453095957, 1);

-- --------------------------------------------------------

--
-- 表的结构 `d_category`
--

CREATE TABLE IF NOT EXISTS `d_category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `category_name` varchar(32) DEFAULT '' COMMENT '分类名称',
  `category_pic` varchar(128) DEFAULT '' COMMENT '头像',
  `category_pic2` varchar(128) DEFAULT '' COMMENT '项目首页小图',
  `category_top` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1默认 2置顶',
  `category_topdate` int(11) NOT NULL COMMENT '置顶日期',
  `category_date` int(11) NOT NULL COMMENT '写入的日期',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='分类表' AUTO_INCREMENT=19 ;

--
-- 转存表中的数据 `d_category`
--

INSERT INTO `d_category` (`category_id`, `category_name`, `category_pic`, `category_pic2`, `category_top`, `category_topdate`, `category_date`) VALUES
(15, '婚姻感情', '20160118110145601.png', '20160118110145197.png', 2, 1453086580, 1453086105),
(16, '事业财运', '20160118110533355.jpg', '20160118110533581.png', 1, 1453086333, 1453086333),
(17, '流年运势', '20160118110934145.png', '20160118110934223.png', 1, 1453086574, 1453086574);

-- --------------------------------------------------------

--
-- 表的结构 `d_check`
--

CREATE TABLE IF NOT EXISTS `d_check` (
  `check_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `check_user_id` int(11) NOT NULL COMMENT '关联user表大师的user_id',
  `check_user_id2` int(11) NOT NULL COMMENT '关联user表客户的user_id',
  PRIMARY KEY (`check_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='私信身份检测表' AUTO_INCREMENT=9 ;

--
-- 转存表中的数据 `d_check`
--

INSERT INTO `d_check` (`check_id`, `check_user_id`, `check_user_id2`) VALUES
(1, 2, 3),
(3, 4, 3),
(5, 5, 6),
(6, 5, 7),
(8, 5, 3);

-- --------------------------------------------------------

--
-- 表的结构 `d_detailed`
--

CREATE TABLE IF NOT EXISTS `d_detailed` (
  `detailed_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `detailed_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0大师账户明细 1客户账户明细',
  `detailed_user_id` int(11) NOT NULL COMMENT '关联user表大师的user_id，如果是大师账户明细则只是查；如果是客户账户明细则需用此user_id查出大师头像、昵称',
  `detailed_user_id2` int(11) NOT NULL,
  `detailed_content` varchar(500) DEFAULT '' COMMENT '交易内容 不管是分类、提现、充值都将以文字形式入库，不写入表关联信息',
  `detailed_money` int(11) NOT NULL DEFAULT '0' COMMENT '交易金额 进账+ 出账- 直接文字入库，不写入表关联信息',
  `detailed_date` int(11) NOT NULL COMMENT '交易日期',
  PRIMARY KEY (`detailed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='账户交易明细表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `d_feedback`
--

CREATE TABLE IF NOT EXISTS `d_feedback` (
  `feedback_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `feedback_user_id` int(11) NOT NULL COMMENT '关联user表的user_id 大师和客户皆可反馈',
  `feedback_content` varchar(500) DEFAULT '' COMMENT '反馈内容',
  `feedback_date` int(11) NOT NULL COMMENT '反馈时间',
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='意见反馈表' AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `d_feedback`
--

INSERT INTO `d_feedback` (`feedback_id`, `feedback_user_id`, `feedback_content`, `feedback_date`) VALUES
(1, 2, '你这个平台有个叫张天师的大师，算卦非常准', 1452849405);

-- --------------------------------------------------------

--
-- 表的结构 `d_goods`
--

CREATE TABLE IF NOT EXISTS `d_goods` (
  `goods_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `goods_user_id` int(11) NOT NULL COMMENT '关联user表大师的user_id',
  `goods_user_id2` int(11) NOT NULL COMMENT '关联user表客户的user_id',
  `goods_state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0试测 1测试（购买） 2已放款  3已评价',
  `goods_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0失败 1成功',
  `goods_sn` varchar(64) NOT NULL COMMENT '订单编号',
  `goods_money` varchar(32) DEFAULT '' COMMENT '交易价格',
  `goods_category_id` int(11) NOT NULL DEFAULT '0' COMMENT '交易类别category_id',
  `goods_project_id` int(11) NOT NULL COMMENT '交易项目project_id',
  `goods_value` varchar(64) DEFAULT '' COMMENT '评分',
  `goods_content` varchar(500) DEFAULT '' COMMENT '评论内容',
  `goods_date` int(11) NOT NULL COMMENT '交易日期',
  PRIMARY KEY (`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单交易表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `d_layer`
--

CREATE TABLE IF NOT EXISTS `d_layer` (
  `layer_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `layer_link` varchar(500) DEFAULT '' COMMENT '超级链接',
  `layer_pic` varchar(256) DEFAULT '' COMMENT '图像路径',
  `layer_top` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1默认 2置顶',
  `layer_topdate` int(11) NOT NULL COMMENT '置顶使用的日期时间 时间戳',
  `layer_date` int(11) NOT NULL COMMENT '写入的日期',
  PRIMARY KEY (`layer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='图片轮播表' AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `d_layer`
--

INSERT INTO `d_layer` (`layer_id`, `layer_link`, `layer_pic`, `layer_top`, `layer_topdate`, `layer_date`) VALUES
(1, 'http://www.baidu.com', '/uploads/201601/1453102377_APP.jpg', 1, 1453170485, 1453170485),
(2, 'http://www.sina.com.cn', '/uploads/201601/1453095957_APP.jpg', 1, 1453170524, 1453170524);

-- --------------------------------------------------------

--
-- 表的结构 `d_message`
--

CREATE TABLE IF NOT EXISTS `d_message` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `message_user_id` int(11) NOT NULL COMMENT '关联user表大师的user_id',
  `message_user_id2` int(11) NOT NULL COMMENT '关联user表客户的user_id',
  `message_content` varchar(500) DEFAULT '' COMMENT '私信内容',
  `message_state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未读 1已读',
  `message_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0客户发送 1大师发送',
  `message_date` int(11) NOT NULL COMMENT '发送日期',
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='私信表' AUTO_INCREMENT=14 ;

--
-- 转存表中的数据 `d_message`
--

INSERT INTO `d_message` (`message_id`, `message_user_id`, `message_user_id2`, `message_content`, `message_state`, `message_status`, `message_date`) VALUES
(1, 2, 3, '这是什么？', 1, 0, 1453370077),
(2, 2, 3, '那是什么？', 1, 1, 1453370046),
(3, 2, 3, '你是什么？', 1, 0, 1453370477),
(4, 4, 3, '我是什么？', 1, 1, 1453370499),
(5, 4, 3, '他是什么？', 0, 0, 1453370516),
(6, 4, 3, '你有什么？', 1, 0, 1453370526),
(7, 5, 3, '我有什么？', 1, 1, 1453370537),
(8, 5, 3, '他有什么？', 1, 0, 1453370548),
(9, 5, 3, '都没什么？', 0, 0, 1453370159),
(12, 5, 3, '我哭了', 0, 0, 1453429052),
(13, 5, 3, '他哭了', 0, 1, 1453432358);

-- --------------------------------------------------------

--
-- 表的结构 `d_money`
--

CREATE TABLE IF NOT EXISTS `d_money` (
  `money_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `money_user_id2` int(11) NOT NULL COMMENT '关联user表客户的user_id',
  `money_num` int(11) NOT NULL DEFAULT '0' COMMENT '充值钱数',
  `money_way` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0支付宝 1微信 2银联',
  `money_date` int(11) NOT NULL COMMENT '充值日期',
  PRIMARY KEY (`money_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户充值表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `d_project`
--

CREATE TABLE IF NOT EXISTS `d_project` (
  `project_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `project_category_id` int(11) NOT NULL COMMENT '关联d_category表category_id',
  `project_user_id` int(11) NOT NULL COMMENT '关联user表大师的user_id',
  `project_title` varchar(32) NOT NULL DEFAULT '' COMMENT '项目标题',
  `project_price` int(11) NOT NULL COMMENT '项目价格',
  `project_content` varchar(500) DEFAULT '' COMMENT '项目详细描述',
  `project_date` int(11) NOT NULL COMMENT '发表日期',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='服务项目表' AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `d_project`
--

INSERT INTO `d_project` (`project_id`, `project_category_id`, `project_user_id`, `project_title`, `project_price`, `project_content`, `project_date`) VALUES
(2, 16, 1, '告诉你富豪的命运', 40, '早知道就早成富豪', 1453088693),
(4, 15, 1, '大龄男女往这里看', 30, '30元给你指出爱的方向在哪里，不准全额退款', 1453089021);

-- --------------------------------------------------------

--
-- 表的结构 `d_scds`
--

CREATE TABLE IF NOT EXISTS `d_scds` (
  `scds_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `scds_user_id` int(11) NOT NULL COMMENT '关联user表大师的user_id',
  `scds_user_id2` int(11) NOT NULL COMMENT '关联user表客户的user_id',
  `scds_date` int(11) NOT NULL COMMENT '收藏的日期',
  PRIMARY KEY (`scds_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户收藏大师表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `d_scwz`
--

CREATE TABLE IF NOT EXISTS `d_scwz` (
  `scwz_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `scwz_article_id` int(11) NOT NULL COMMENT '文章article_id',
  `scwz_user_id2` int(11) NOT NULL COMMENT '关联user表客户的user_id',
  `scwz_date` int(11) NOT NULL COMMENT '收藏的日期',
  PRIMARY KEY (`scwz_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户收藏文章表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `d_speciality`
--

CREATE TABLE IF NOT EXISTS `d_speciality` (
  `speciality_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `speciality_category_id` int(11) NOT NULL COMMENT '关联d_category表category_id',
  `speciality_user_id` int(11) NOT NULL COMMENT '关联user表大师的user_id',
  PRIMARY KEY (`speciality_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='擅长表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `d_talk`
--

CREATE TABLE IF NOT EXISTS `d_talk` (
  `talk_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `talk_user_id` int(11) NOT NULL COMMENT '关联user表客户的user_id 查出客户的头像、昵称',
  `talk_article_id` int(11) NOT NULL COMMENT '关联文章表article_id',
  `talk_content` varchar(500) DEFAULT '' COMMENT '评论内容',
  `talk_date` int(11) NOT NULL COMMENT '评论日期 时间戳',
  PRIMARY KEY (`talk_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='对大师文章评论表' AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `d_talk`
--

INSERT INTO `d_talk` (`talk_id`, `talk_user_id`, `talk_article_id`, `talk_content`, `talk_date`) VALUES
(1, 1, 1, '偏印确实都是计算机天才', 1453097614),
(2, 1, 1, '偏印的学术要比正印高深的多', 1453097633),
(3, 1, 2, '正宫的学术要比正印高深的多', 1453097633),
(4, 1, 2, '正宫一般都工作在政府', 1453097633);

-- --------------------------------------------------------

--
-- 表的结构 `d_type`
--

CREATE TABLE IF NOT EXISTS `d_type` (
  `type_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `type_name` varchar(32) CHARACTER SET utf8 DEFAULT '' COMMENT '分类名称',
  `type_pic` varchar(128) CHARACTER SET utf8 DEFAULT '' COMMENT '图像',
  `type_top` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1默认 2置顶',
  `type_topdate` int(11) NOT NULL COMMENT '置顶日期',
  `type_date` int(11) NOT NULL COMMENT '写入的日期',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='文章分类表' AUTO_INCREMENT=9 ;

--
-- 转存表中的数据 `d_type`
--

INSERT INTO `d_type` (`type_id`, `type_name`, `type_pic`, `type_top`, `type_topdate`, `type_date`) VALUES
(4, '命理', '20160118111021136.png', 2, 1453347169, 1453086621),
(5, '风水', '20160118111042456.png', 2, 1453086691, 1453086642),
(6, '生肖', '20160118111054691.png', 2, 1453347163, 1453086654),
(7, '励志', '20160118111108194.png', 2, 1453347147, 1453086668);

-- --------------------------------------------------------

--
-- 表的结构 `d_user`
--

CREATE TABLE IF NOT EXISTS `d_user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_identity_id` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0客户 1大师',
  `user_online` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0离线 1在线 2忙碌 只有大师分这三种状态',
  `user_money` bigint(20) NOT NULL DEFAULT '0' COMMENT '余额',
  `user_votes` bigint(20) NOT NULL DEFAULT '0' COMMENT '评论人数',
  `user_value` bigint(20) NOT NULL DEFAULT '0' COMMENT '评论总分数',
  `user_pct` decimal(6,1) NOT NULL DEFAULT '0.0' COMMENT '评论百分比',
  `user_pic` varchar(128) NOT NULL DEFAULT 'pic.jpg' COMMENT '头像',
  `user_tel` varchar(32) DEFAULT '' COMMENT '电话',
  `user_speciality` varchar(500) DEFAULT '' COMMENT '擅长的category_id集群 例如：1,2,33,45',
  `user_pwd` varchar(32) DEFAULT '' COMMENT '密码 手机端直接传递加密密码',
  `user_link` varchar(32) DEFAULT '' COMMENT '明文密码',
  `user_username` varchar(32) DEFAULT '' COMMENT '昵称',
  `user_sex` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0保密 1男 2女',
  `user_broken` varchar(256) DEFAULT '' COMMENT '个性签名',
  `user_content` varchar(500) DEFAULT '' COMMENT '个人简介',
  `user_content_pic` text COMMENT '个人简介中的图片（支持多张）',
  `user_name` varchar(32) DEFAULT '' COMMENT '持卡人姓名',
  `user_card` varchar(64) DEFAULT '' COMMENT '银行卡账号',
  `user_ccb` varchar(64) DEFAULT '' COMMENT '哪个银行',
  `user_top` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1默认 2置顶  注意：这里有个大师置顶',
  `user_topdate` int(11) NOT NULL COMMENT '置顶使用的日期时间 时间戳',
  `user_date` int(11) NOT NULL COMMENT '大师、客户入驻平台时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='用户表' AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `d_user`
--

INSERT INTO `d_user` (`user_id`, `user_identity_id`, `user_online`, `user_money`, `user_votes`, `user_value`, `user_pct`, `user_pic`, `user_tel`, `user_speciality`, `user_pwd`, `user_link`, `user_username`, `user_sex`, `user_broken`, `user_content`, `user_content_pic`, `user_name`, `user_card`, `user_ccb`, `user_top`, `user_topdate`, `user_date`) VALUES
(1, 1, 0, 660, 116, 579, '99.9', '1452848842_APP.jpg', '18519011617', '16,', '96e79218965eb72c92a549dd5a330112', '111111', '朱元璋', 1, '精通易经、八字、奇门遁甲之术！3分钟看透你的人生，不准不收款！', '年龄66岁，跟随12个师傅学艺，有易学奇才之称号，看命无数，唯独看错过一次，皆因对方提供了错误的生辰资料！', NULL, '猪八戒', '632788993098787', '中国农业银行-北京市上地支行', 2, 1453182075, 1453097614),
(2, 1, 0, 700, 12, 59, '99.2', '20160120112529585.png', '18519011617', '15,16,', 'e10adc3949ba59abbe56e057f20f883e', '123456', '刘亚玲', 2, '我是世界上最不要脸的人', '我的腰犹如老树盘根！', './uploads/201601/14531095040_APP.jpg,./uploads/201601/14531095041_APP.jpg20160120184538975.jpg,', '刘老根', '6210789299092211', '中国建设银行北京市海淀区上地支行', 2, 1453182079, 1453086333),
(3, 1, 0, 0, 0, 0, '0.0', 'pic.jpg', '18529088979', '15,', '96e79218965eb72c92a549dd5a330112', '111111', '马云', 0, '', '', NULL, '', '', '', 1, 1453458532, 1453345033),
(4, 1, 0, 0, 0, 0, '0.0', 'pic.jpg', '18529088979', '15,', '96e79218965eb72c92a549dd5a330112', '111111', '刘强东', 0, '', '', NULL, '', '', '', 1, 0, 1453345033),
(5, 1, 0, 0, 0, 0, '0.0', 'pic.jpg', '18529088979', '15,', '96e79218965eb72c92a549dd5a330112', '111111', '马化腾', 0, '', '', NULL, '', '', '', 1, 0, 1453345033);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
